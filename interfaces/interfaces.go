package interfaces

// CLIOptions is a struct containing cli options
type CLIOptions struct {
	CfgFile string

	Host          string
	Model         string
	Output        string
	Verbose       bool
	LoginUser     string
	LoginPassword string
	Command       string
	Subcommand    string

	Version string

	// ACL
	ACLFile    string
	Rule       string
	RuleNumber int
	Policy     string

	// User
	Username    string
	NewUsername string
	Password    string
	Permission  string

	// TLS
	Key  string
	Cert string

	// Time
	Timezone string
	DST      string
	NTP      string
	NTPServ1 string
	NTPServ2 string
	Date     string
	Time     string

	// Network
	DNS string
}
