package cmd

import (
	"github.com/spf13/cobra"
)

// timeCmd represents the time command
var timeCmd = &cobra.Command{
	Use:   "time",
	Short: "Display or update IPMI time settings",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var timeListCmd = &cobra.Command{
	Use:   "list",
	Short: "List current time settings",
	Run: func(cmd *cobra.Command, args []string) {
		prepareOptsAndStart("time", "list")
	},
}

var timeUpdateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update time settings",
	Run: func(cmd *cobra.Command, args []string) {
		prepareOptsAndStart("time", "update")
	},
}

func init() {
	// Update
	timeUpdateCmd.Flags().StringVar(&timezone, "timezone", "", "IPMI timezone. Ex: [1, -2]")
	timeUpdateCmd.Flags().StringVar(&dst, "dst", "", "Use daylight savings time? [true - false]")
	timeUpdateCmd.Flags().StringVar(&ntp, "ntp", "", "Use NTP? [true - false]")
	timeUpdateCmd.Flags().StringVar(&ntpServ1, "ntpserv1", "", "NTP server 1")
	timeUpdateCmd.Flags().StringVar(&ntpServ2, "ntpserv2", "", "NTP server 2")
	timeUpdateCmd.Flags().StringVar(&date, "date", "", "Date in [YYYY-MM-DD] format")
	timeUpdateCmd.Flags().StringVar(&time, "time", "", "Time in [HH:MM:SS] format")

	// Add commands to timeCmd
	timeCmd.AddCommand(timeListCmd)
	timeCmd.AddCommand(timeUpdateCmd)

	// Add to rootCmd
	rootCmd.AddCommand(timeCmd)
}
