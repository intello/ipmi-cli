package cmd

import "github.com/spf13/cobra"

// rebootCmd represents the reboot command
var rebootCmd = &cobra.Command{
	Use:   "reboot",
	Short: "Reboot your IPMI",
	Run: func(cmd *cobra.Command, args []string) {
		prepareOptsAndStart("reboot", "")
	},
}

func init() {
	// Add to rootCmd
	rootCmd.AddCommand(rebootCmd)
}
