package cmd

import (
	"github.com/spf13/cobra"
)

// userCmd represents the user command
var userCmd = &cobra.Command{
	Use:   "user",
	Short: "List, create, update or delete and IPMI user",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var userListCmd = &cobra.Command{
	Use:   "list",
	Short: "List, create, update or delete and IPMI user",
	Run: func(cmd *cobra.Command, args []string) {
		prepareOptsAndStart("user", "list")
	},
}

var userCreateCmd = &cobra.Command{
	Use:   "create",
	Short: "Create a new IPMI user",
	Run: func(cmd *cobra.Command, args []string) {
		if permission == "" {
			permission = "Administrator"
		}

		prepareOptsAndStart("user", "create")
	},
}

var userUpdateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update an IPMI user",
	Run: func(cmd *cobra.Command, args []string) {
		prepareOptsAndStart("user", "update")
	},
}

var userDeleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Delete an IPMI user",
	Run: func(cmd *cobra.Command, args []string) {
		prepareOptsAndStart("user", "delete")
	},
}

func init() {
	// Flags

	// Create
	userCreateCmd.Flags().StringVarP(&username, "username", "", "", "Username for new IPMI user")
	userCreateCmd.Flags().StringVarP(&password, "password", "", "", "password for new IPMI user")
	userCreateCmd.Flags().StringVarP(&permission, "permission", "", "Administrator", "permission for new IPMI user")

	// Update
	userUpdateCmd.Flags().StringVarP(&username, "username", "", "", "Username for IPMI user to update")
	userUpdateCmd.Flags().StringVarP(&newusername, "newusername", "", "", "New username for IPMI user to update")
	userUpdateCmd.Flags().StringVarP(&password, "password", "", "", "Password for IPMI user to update (can stay the same)")
	userUpdateCmd.Flags().StringVarP(&permission, "permission", "", "", "Permission for IPMI user to update")

	// Delete
	userDeleteCmd.Flags().StringVarP(&username, "username", "", "", "Username for IPMI user to delete") // TODO: Fix this

	// Required flags

	// Create
	userCreateCmd.MarkFlagRequired("username")
	userCreateCmd.MarkFlagRequired("password")

	// Update
	userUpdateCmd.MarkFlagRequired("username")
	userUpdateCmd.MarkFlagRequired("password")
	userUpdateCmd.MarkFlagRequired("permission")

	// Delete
	userDeleteCmd.MarkFlagRequired("username")

	// Add commands to userCmd
	userCmd.AddCommand(userListCmd)
	userCmd.AddCommand(userCreateCmd)
	userCmd.AddCommand(userUpdateCmd)
	userCmd.AddCommand(userDeleteCmd)

	// Add to rootCmd
	rootCmd.AddCommand(userCmd)
}
