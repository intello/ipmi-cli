package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// aclCmd represents the acl command
var aclCmd = &cobra.Command{
	Use:   "acl",
	Short: "Set, display or update IPMI ACL rules",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var aclListCmd = &cobra.Command{
	Use:   "list",
	Short: "List current ACL rules",
	Run: func(cmd *cobra.Command, args []string) {
		prepareOptsAndStart("acl", "list")
	},
}

var aclUpdateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update a rule in the list of ACL rules",
	Run: func(cmd *cobra.Command, args []string) {
		prepareOptsAndStart("acl", "update")
	},
}

var aclSetCmd = &cobra.Command{
	Use:   "set",
	Short: "Wipe existing ACL rules and sets new ones from a file",
	Run: func(cmd *cobra.Command, args []string) {
		prepareOptsAndStart("acl", "set")
	},
}

var aclDeleteCmd = &cobra.Command{
	Use:   "delete",
	Short: "Wipe an existing rule",
	Run: func(cmd *cobra.Command, args []string) {
		prepareOptsAndStart("acl", "delete")
	},
}

var aclDeleteAllCmd = &cobra.Command{
	Use:   "deleteall",
	Short: "Wipe all existing rules",
	Run: func(cmd *cobra.Command, args []string) {
		prepareOptsAndStart("acl", "deleteall")
	},
}

func init() {
	// Flags

	// List
	aclListCmd.Flags().StringVarP(&saveACL, "save", "s", "", "Save ACL to specified folder ('.' for current directory)")

	// Update
	aclUpdateCmd.Flags().IntVarP(&ruleno, "ruleno", "", -1, "Rule number to modify")
	aclUpdateCmd.Flags().StringVarP(&rule, "rule", "", "", "New IPv4 CIDR rule")
	aclUpdateCmd.Flags().StringVarP(&policy, "policy", "", "", "Policy for new rule")

	// Set
	aclSetCmd.Flags().StringVar(&aclfile, "aclfile", "", "File containing new ACL rules")

	// Delete
	aclDeleteCmd.Flags().IntVarP(&ruleno, "ruleno", "", -1, "ACL Rule number to delete") // TODO: Fix this

	// Required flags

	// Update
	aclUpdateCmd.MarkFlagRequired("ruleno")
	aclUpdateCmd.MarkFlagRequired("rule")
	aclUpdateCmd.MarkFlagRequired("policy")

	// Delete
	aclDeleteCmd.MarkFlagRequired("ruleno")

	// Bind Flags
	viper.BindPFlag("aclfile", aclSetCmd.Flags().Lookup("aclfile"))

	// Add commands to aclCmd
	aclCmd.AddCommand(aclListCmd)
	aclCmd.AddCommand(aclSetCmd)
	aclCmd.AddCommand(aclUpdateCmd)
	aclCmd.AddCommand(aclDeleteCmd)
	aclCmd.AddCommand(aclDeleteAllCmd)

	// Add to rootCmd
	rootCmd.AddCommand(aclCmd)
}
