package cmd

import (
	"github.com/spf13/cobra"
)

// networkCmd represents the network command
var networkCmd = &cobra.Command{
	Use:   "network",
	Short: "Display or update IPMI network settings",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var networkListCmd = &cobra.Command{
	Use:   "list",
	Short: "List current network settings",
	Run: func(cmd *cobra.Command, args []string) {
		prepareOptsAndStart("network", "list")
	},
}

var networkUpdateCmd = &cobra.Command{
	Use:   "update",
	Short: "Update network settings",
	Run: func(cmd *cobra.Command, args []string) {
		prepareOptsAndStart("network", "update")
	},
}

func init() {
	// Update
	networkUpdateCmd.Flags().StringVar(&dns, "dns", "", "IPv4 DNS IP - [EX: 8.8.8.8]")

	// Add commands to timeCmd
	networkCmd.AddCommand(networkListCmd)
	networkCmd.AddCommand(networkUpdateCmd)

	// Add to rootCmd
	rootCmd.AddCommand(networkCmd)
}
