package cmd

import (
	"bufio"
	"fmt"
	"ipmi-cli/errhandler"
	"os"
	"os/user"
	"strings"

	"github.com/fatih/color"

	"github.com/spf13/cobra"
	"gopkg.in/yaml.v2"
)

// In order to create configuration file
type conf struct {
	Loginuser     string `yaml:"loginuser"`
	Loginpassword string `yaml:"loginpassword"`
	Output        string `yaml:"output"`
	ACLFile       string `yaml:"aclfile"`
	Key           string `yaml:"key"`
	Certificate   string `yaml:"cert"`
}

// configCmd represents the config command
var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Setup your ipmi-cli configuration file",
	Run: func(cmd *cobra.Command, args []string) {
		startSetup()
	},
}

func init() {
	// Add to rootCmd
	rootCmd.AddCommand(configCmd)
}

func startSetup() {
	config := &conf{}

	reader := bufio.NewReader(os.Stdin)

	// Grab homepath
	myself, error := user.Current()
	if error != nil {
		panic(error)
	}
	homedir := myself.HomeDir

	if cfgFile == "" {
		// No -c provided
		cfgFile = homedir + string(os.PathSeparator) + ".ipmi-cli.yml"
	}

	if fileExists(cfgFile) {
		reader := bufio.NewReader(os.Stdin)

		clearConsole()

		for {
			fmt.Print(color.RedString("\n***\nA configuration file already exists, do you want to overwrite it?\n(y)es or (n)o : "))
			answer, _ := reader.ReadString('\n')
			answer = strings.ToLower(strings.TrimRight(answer, "\r\n"))

			if answer == "n" || answer == "no" {
				fmt.Println(color.GreenString("Aborted configuration"))
				return
			} else if answer == "y" || answer == "yes" {
				break
			}

			fmt.Print(color.CyanString("Please only answer with 'y' or 'n'"))
		}
	}

	// Ask questions to build config file
	// Username
	fmt.Print(color.CyanString("\n***\nUsername for IPMI : "))
	config.Loginuser, _ = reader.ReadString('\n')
	config.Loginuser = strings.Replace(config.Loginuser, "\n", "", -1)
	config.Loginuser = strings.Replace(config.Loginuser, "\r", "", -1)

	// Password
	fmt.Print(color.CyanString("\nPassword for IPMI : "))
	config.Loginpassword, _ = reader.ReadString('\n')
	config.Loginpassword = strings.Replace(config.Loginpassword, "\n", "", -1)
	config.Loginpassword = strings.Replace(config.Loginpassword, "\r", "", -1)

	// Output
	for {
		fmt.Print(color.CyanString("\nOutput format - Leave empty for default (text)\n  Available formats are: [text, json] : "))
		config.Output, _ = reader.ReadString('\n')
		config.Output = strings.Replace(config.Output, "\n", "", -1)
		config.Output = strings.Replace(config.Output, "\r", "", -1)

		config.Output = strings.ToLower(config.Output)

		if config.Output == "" {
			config.Output = "text"
		}

		if config.Output == "text" || config.Output == "json" {
			break
		}

		fmt.Print(color.RedString("Please only answer with ['', 'text' or 'json']\n"))
	}

	// ACLFile
	fmt.Print(color.CyanString("\n[acl set] Path to rules JSON file - empty to skip : "))
	config.ACLFile, _ = reader.ReadString('\n')
	config.ACLFile = strings.Replace(config.ACLFile, "\n", "", -1)
	config.ACLFile = strings.Replace(config.ACLFile, "\r", "", -1)

	// Key
	fmt.Print(color.CyanString("\n[tls update] Path to key file - empty to skip : "))
	config.Key, _ = reader.ReadString('\n')
	config.Key = strings.Replace(config.Key, "\n", "", -1)
	config.Key = strings.Replace(config.Key, "\r", "", -1)

	// Cert
	fmt.Print(color.CyanString("\n[tls update] Path to certificate file - empty to skip : "))
	config.Certificate, _ = reader.ReadString('\n')
	config.Certificate = strings.Replace(config.Certificate, "\n", "", -1)
	config.Certificate = strings.Replace(config.Certificate, "\r", "", -1)

	// Create yaml data
	confData, err := yaml.Marshal(config)
	errhandler.ExitIfErr(err)

	createConfigFile(confData, homedir)
}

func createConfigFile(configData []byte, homedir string) {
	// Delete and/or create file
	os.Remove(cfgFile)
	f, err := os.OpenFile(cfgFile, os.O_CREATE|os.O_WRONLY, 0644)
	errhandler.ExitIfErr(err)

	_, err = f.Write(configData)
	errhandler.ExitIfErr(err)

	f.Close()

	fmt.Print(color.GreenString("\nConfiguration file created at %s\n", cfgFile))
}

// fileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func clearConsole() {
	fmt.Print("\033[H\033[2J")
}
