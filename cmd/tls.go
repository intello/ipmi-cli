package cmd

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// tlsCmd represents the tls command
var tlsCmd = &cobra.Command{
	Use:   "tls",
	Short: "Upload SSL certificate and key",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var tlsUpdateCmd = &cobra.Command{
	Use:   "update",
	Short: "Upload SSL certificate and key",
	Run: func(cmd *cobra.Command, args []string) {
		prepareOptsAndStart("tls", "update")
	},
}

func init() {
	// Flags

	// Update
	tlsUpdateCmd.Flags().StringVar(&key, "key", "", "SSL certificate key file")
	tlsUpdateCmd.Flags().StringVar(&cert, "cert", "", "SSL certificate file")

	// Bind Flags
	viper.BindPFlag("key", tlsUpdateCmd.Flags().Lookup("key"))
	viper.BindPFlag("cert", tlsUpdateCmd.Flags().Lookup("cert"))

	// Add commands to tlsCmd
	tlsCmd.AddCommand(tlsUpdateCmd)

	// Add to rootCmd
	rootCmd.AddCommand(tlsCmd)
}
