package cmd

import (
	"fmt"
	"io/ioutil"
	"ipmi-cli/errhandler"
	"ipmi-cli/interfaces"
	"ipmi-cli/logger"
	"ipmi-cli/models"
	"ipmi-cli/optshandler"
	"ipmi-cli/softwareversion"
	"os"
	"strings"

	"github.com/fatih/color"
	"github.com/tidwall/pretty"

	"github.com/sirupsen/logrus"

	"github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	// Global flags
	cfgFile       string
	host          string
	loginuser     string
	loginpassword string
	model         string
	output        string
	verbose       bool

	// Acl list
	saveACL string

	// Acl update
	ruleno int
	rule   string
	policy string

	// Acl set
	aclfile string

	// User create
	username   string
	password   string
	permission string

	// User update
	newusername string

	// TLS update
	key  string
	cert string

	// Time
	timezone string
	dst      string
	ntp      string
	ntpServ1 string
	ntpServ2 string
	date     string
	time     string

	// Network
	dns string

	rootCmd = &cobra.Command{
		Use:     "ipmi-cli",
		Short:   "IPMI setup allows you to configure your IPMIs the easy way!",
		Version: softwareversion.Version,
	}
)

var conFigFileUsed bool = false

// Execute is the entry point of the CMD
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file (default is $HOME/.ipmi-cli.yml)")
	rootCmd.PersistentFlags().StringVar(&host, "host", "", "Host of the IPMI (Required)")
	rootCmd.PersistentFlags().StringVar(&loginuser, "loginuser", "", "Username used to log into the IPMI (Required)")
	rootCmd.PersistentFlags().StringVar(&loginpassword, "loginpassword", "", "Password used to log into the IPMI (Required)")
	rootCmd.PersistentFlags().StringVar(&model, "model", "supermicro", "Model of the IPMI")
	rootCmd.PersistentFlags().StringVar(&output, "output", "text", "Output format\n  Possible values: [text, json]")
	rootCmd.PersistentFlags().BoolVar(&verbose, "verbose", false, "Verbose mode")

	// Bind on flags that can come from the config file
	viper.BindPFlag("loginuser", rootCmd.PersistentFlags().Lookup("loginuser"))
	viper.BindPFlag("loginpassword", rootCmd.PersistentFlags().Lookup("loginpassword"))
	viper.BindPFlag("output", rootCmd.PersistentFlags().Lookup("output"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		viper.AddConfigPath(home)
		viper.SetConfigName(".ipmi-cli")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		conFigFileUsed = true
	}
}

func buildOpts(command string, subcommand string) *interfaces.CLIOptions {
	loginuser = viper.GetString("loginuser")
	loginpassword = viper.GetString("loginpassword")
	key = viper.GetString("key")
	cert = viper.GetString("cert")
	output = strings.ToLower(viper.GetString("output"))
	aclfile = viper.GetString("aclfile")

	if verbose {
		logger.Init(logrus.InfoLevel.String())
	}

	opts := &interfaces.CLIOptions{
		Command:       command,
		Subcommand:    subcommand,
		Host:          host,
		LoginUser:     loginuser,
		LoginPassword: loginpassword,
		Model:         model,
		Output:        output,

		// Acl update
		RuleNumber: ruleno,
		Rule:       rule,
		Policy:     policy,

		// Acl set
		ACLFile: aclfile,

		// User create
		Username:   username,
		Password:   password,
		Permission: permission,

		// User update
		NewUsername: newusername,

		// TLS update
		Key:  key,
		Cert: cert,

		// Time
		Timezone: timezone,
		DST:      dst,
		NTP:      ntp,
		NTPServ1: ntpServ1,
		NTPServ2: ntpServ2,
		Time:     time,
		Date:     date,

		// Network
		DNS: dns,
	}

	return opts
}

// startLogic will send opts to model handler and start the logic associated with the command
func startLogic(opts *interfaces.CLIOptions) {
	result, err := models.Handler(opts)
	if err != nil {
		logger.Log(color.RedString("%v\n", err.Error()), logrus.FatalLevel)
	}

	// Print result of command
	fmt.Print(color.GreenString("%v\n", result))

	// If saveACL is not empty, we want to save the result to a file
	// Note: This could be global, not necesserily just for acl list
	if saveACL != "" {
		saveToFile(result)
	}
}

func showHelpIfNoArgs(cmd *cobra.Command, args []string) {
	if len(args) == 0 {
		cmd.Help()
		os.Exit(0)
	}
}

func prepareOptsAndStart(command string, subcommand string) {
	opts := buildOpts(command, subcommand)
	err := optshandler.VerifyRequiredParams(opts)
	errhandler.ExitIfErr(err)

	if conFigFileUsed && output != "json" {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}

	startLogic(opts)
}

func saveToFile(result string) {
	var err error
	var resultToSave string
	isDir := fileLocationIsFolder(saveACL)
	fileName := determineFileName()

	// If it's JSON and we want to save, pretty print it!
	if strings.HasSuffix(fileName, ".json") {
		resultToSave = string(pretty.Pretty([]byte(result)))
	} else if strings.HasSuffix(fileName, ".txt") {
		resultToSave = result
	}

	// If the save location is a folder, go ahead and create the file there.
	if isDir {
		err = ioutil.WriteFile(saveACL+string(os.PathSeparator)+fileName, []byte(resultToSave), 0755)
	} else if saveACL == "" {
		// If the saveLocation is empty, this means we should save to the current directory
		err = ioutil.WriteFile(fileName, []byte(resultToSave), 0755)
	}

	if err != nil {
		fmt.Printf(color.RedString("Unable to write file: %v\n", err))
	} else {
		fmt.Printf(color.GreenString("File saved to: %s\n", saveACL+string(os.PathSeparator)+fileName))
	}
}

func fileLocationIsFolder(location string) bool {
	// if the save location is empty, no need to check for path, etc...
	if saveACL != "" {
		// Check if the save location given exists.
		if fileInfo, err := os.Stat(saveACL); os.IsNotExist(err) {
			fmt.Printf(color.RedString("The save location provided does not exist. Results will not be saved.\n"))
		} else {
			// If it exists, we want to verify that the location is a folder.
			if fileInfo.IsDir() {
				return true
			}
		}
	}

	return false
}

func determineFileName() string {
	switch strings.ToLower(output) {
	case "text":
		return "saved_acl.txt"
	case "json":
		return "saved_acl.json"
	default:
		return ""
	}
}
