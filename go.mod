module ipmi-cli

go 1.14

require (
	github.com/fatih/color v1.9.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/cobra v1.0.0
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.4.0
	github.com/tidwall/pretty v1.0.2
	golang.org/x/net v0.0.0-20190522155817-f3200d17e092
	gopkg.in/yaml.v2 v2.2.2
)
