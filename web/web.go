package web

import (
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"ipmi-cli/logger"
	"net/http"

	"github.com/fatih/color"

	"github.com/sirupsen/logrus"
)

// Response contains the HTTP reponse code, HTML body
// and cookies associated with a request
type Response struct {
	HTMLCode int
	Body     string
	Cookies  []*http.Cookie
}

// ClientOptions defines the currently supported options
type ClientOptions struct {
	BaseURL               string
	APIPath               string
	Username              string
	Password              string
	AuthMethod            string
	AuthBearer            string
	Cookies               map[string]string
	ContentType           string
	CertificateValidation bool // Is false by default
	CSRFToken             string
}

var client = http.Client{}
var options = &ClientOptions{}

// Init initializes a new web client
func Init(params ClientOptions) {
	if params.BaseURL == "" {
		err := errors.New("BaseURL needs a value when initializing")
		fatalErr(err)
	}

	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	if !params.CertificateValidation {
		params.BaseURL = "http://" + params.BaseURL
	} else {
		params.BaseURL = "https://" + params.BaseURL
	}

	options.BaseURL = params.BaseURL
	options.APIPath = params.APIPath
	options.Username = params.Username
	options.Password = params.Password
	options.AuthMethod = params.AuthMethod
	options.AuthBearer = params.AuthBearer
	options.Cookies = params.Cookies
	options.ContentType = params.ContentType
	options.CertificateValidation = params.CertificateValidation
}

// Get makes an http call to baseURL/apiPath
func Get(endpoint string) (Response, error) {
	url := fmt.Sprintf("%v/%v/%v", options.BaseURL, options.APIPath, endpoint)
	req, _ := http.NewRequest(http.MethodGet, url, nil)

	// Add all cookies passed on Init
	for cookie, value := range options.Cookies {
		req.AddCookie(&http.Cookie{Name: cookie, Value: value})
	}

	// add authentication headers
	if options.CSRFToken != "" {
		req.Header.Set("CSRF_TOKEN", options.CSRFToken)
		req.Header.Set("Referer", options.BaseURL)
		// for AsRock compatibility
		req.Header.Set("X-CSRFTOKEN", options.CSRFToken)
	}

	if options.AuthMethod == "BasicAuth" {
		logger.Log(
			fmt.Sprintf("Performing a GET on [%s] using BasicAuth with username [%s] and password [%s]\n",
				url, options.Username, options.Password),
			logrus.InfoLevel)

		req.SetBasicAuth(options.Username, options.Password)
	} else {
		logger.Log(fmt.Sprintf("Perfoming a GET on [%s]", url), logrus.InfoLevel)
	}

	logger.Log(fmt.Sprintf("Headers:"), logrus.InfoLevel)
	for name, values := range req.Header {
		for _, value := range values {
			logger.Log(fmt.Sprintf("Name: %s Value %s", name, value), logrus.InfoLevel)
		}
	}

	resp, err := client.Do(req)
	fatalErr(err)

	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()

	return Response{
		HTMLCode: resp.StatusCode,
		Body:     string(body),
		Cookies:  resp.Cookies(),
	}, err
}

// Post makes a post call with body values
func Post(endpoint string, data io.Reader) (Response, error) {
	url := fmt.Sprintf("%v/%v/%v", options.BaseURL, options.APIPath, endpoint)

	// Create new post request and set Content-Type header
	req, _ := http.NewRequest(http.MethodPost, url, data)
	req.Header.Set("Content-Type", options.ContentType)
	if options.CSRFToken != "" {
		req.Header.Set("CSRF_TOKEN", options.CSRFToken)
		req.Header.Set("Referer", options.BaseURL)
		// for AsRock compatibility
		req.Header.Set("X-CSRFTOKEN", options.CSRFToken)
	}

	// Add all cookies passed on Init
	for cookie, value := range options.Cookies {
		req.AddCookie(&http.Cookie{Name: cookie, Value: value})
	}

	if options.AuthMethod == "BasicAuth" {
		logger.Log(
			fmt.Sprintf("Performing a POST on [%s] using BasicAuth with username [%s] and password [%s]\n",
				url, options.Username, options.Password),
			logrus.InfoLevel)

		req.SetBasicAuth(options.Username, options.Password)
	} else {
		logger.Log(fmt.Sprintf("Perfoming a POST on [%s]", url), logrus.InfoLevel)
	}

	logger.Log(fmt.Sprintf("Headers:"), logrus.InfoLevel)
	for name, values := range req.Header {
		for _, value := range values {
			logger.Log(fmt.Sprintf("Name: %s Value %s", name, value), logrus.InfoLevel)
		}
	}

	resp, err := client.Do(req)
	if err != nil {
		// For a prettier error message
		err = fmt.Errorf("Unable to reach host: \"%v\"", options.BaseURL)
		fatalErr(err)
	}

	body, err := ioutil.ReadAll(resp.Body)
	resp.Body.Close()

	logger.Log(fmt.Sprintf("Got response: [%s]", string(body)), logrus.InfoLevel)
	logger.Log(fmt.Sprintf("Headers:"), logrus.InfoLevel)
	for name, values := range resp.Header {
		for _, value := range values {
			logger.Log(fmt.Sprintf("Name: %s Value %s", name, value), logrus.InfoLevel)
		}
	}

	return Response{
		HTMLCode: resp.StatusCode,
		Body:     string(body),
		Cookies:  resp.Cookies(),
	}, err
}

// fatalErr checks for an error
func fatalErr(err error) {
	if err != nil {
		logger.Log(color.RedString(err.Error()), logrus.FatalLevel)
	}
}

// AddCookie will add a key/value cookie to the options
func AddCookie(name string, value string) {
	options.Cookies[name] = value
}

// ChangeCSRFToken is will set a CSRF in the headers of a request
func ChangeCSRFToken(value string) {
	options.CSRFToken = value
	logger.Log("CSRF Token set to: "+value, logrus.InfoLevel)
}

// GetCSRFToken will return the current CSRFToken
func GetCSRFToken() string {
	return options.CSRFToken
}

// ChangeContentType will change the content type from the client
func ChangeContentType(contentType string) {
	options.ContentType = contentType
}
