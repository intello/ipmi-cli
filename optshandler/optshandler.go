package optshandler

import (
	"fmt"
	"ipmi-cli/interfaces"
	"reflect"
	"strings"
)

// globally required params need to be entered here. Case insensitive.
var globalParams = []string{"host", "loginuser", "loginpassword"}
var tlsUpdateParams = []string{"key", "cert"}
var aclSetParams = []string{"aclfile"}

// VerifyRequiredParams verifies the parameters that are required for certain operations
func VerifyRequiredParams(opts *interfaces.CLIOptions) error {
	var errArr []error

	newErr := checkIfRequiredParamsAreMissing(opts, globalParams)
	errArr = append(errArr, newErr)

	if opts.Command == "tls" && opts.Subcommand == "update" {
		newErr := checkIfRequiredParamsAreMissing(opts, tlsUpdateParams)
		errArr = append(errArr, newErr)
	} else if opts.Command == "acl" && opts.Subcommand == "set" {
		newErr := checkIfRequiredParamsAreMissing(opts, aclSetParams)
		errArr = append(errArr, newErr)
	}

	err := assembleErrorMessage(errArr)

	return err
}

func checkIfRequiredParamsAreMissing(opts *interfaces.CLIOptions, paramArray []string) error {
	var err error = nil
	var requiredString string = ""

	// https://stackoverflow.com/questions/18926303/iterate-through-the-fields-of-a-struct-in-go
	v := reflect.ValueOf(*opts)
	typeOfS := v.Type()

	// Put everything to lowercase to avoid confusion
	paramArray = lowerCaseArray(paramArray)

	for i := 0; i < v.NumField(); i++ {
		field := strings.ToLower(typeOfS.Field(i).Name)

		if stringInSlice(field, paramArray) {
			if v.Field(i).Interface() == "" {
				requiredString += field + ", "
			}
		}
	}

	if requiredString != "" {
		err = fmt.Errorf(requiredString)
	}

	return err
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func lowerCaseArray(list []string) []string {
	returnString := list

	for i, value := range list {
		returnString[i] = strings.ToLower(value)
	}

	return returnString
}

func assembleErrorMessage(err []error) error {
	var errString string

	for _, errValue := range err {
		if errValue != nil {
			errString += errValue.Error()
		}
	}

	errString = strings.TrimSuffix(errString, ", ")

	if errString == "" {
		return nil
	}

	return fmt.Errorf("Required params :\n  %s are not set", errString)
}
