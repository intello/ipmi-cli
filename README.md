# ipmi-cli

[![pipeline status](https://gitlab.com/intello/ipmi-cli/badges/master/pipeline.svg)](https://gitlab.com/intello/ipmi-cli/-/commits/master)[![coverage report](https://gitlab.com/intello/ipmi-cli/badges/master/coverage.svg)](https://gitlab.com/intello/ipmi-cli/-/commits/master)

## What problem(s) does it solve?

A lot of Supermicro motherboard (amonst others) have a feature rich IPMI implementation that can only be managed via s Web interface. This limits the automation efforts and is prone to errors when configuring large number of servers.

There was a need internally at Intello Technologies to be able to audit the servers IPMI configuration and make sure they were all configured the same during the staging process, so `ipmi-cli` was born.

## What will `ipmi-cli` do and not do

### `ipmi-cli` will :

- Set the ACL rules according to a predefined configuration.
- Allow you to upload certificates.
- User managment:
	- List the current users
	- Create, modify, delete users
	- Change a user password

### `ipmi-cli` will not :

- Allow you to control server via the IPMI KVM
- Read IPMI sensors (use `ipmitool` for this)
- Allow you to shut down or restart the server (this can also be done via the `ipmitool` utility)

## Supported motherboard and IPMI firmware versions

`ipmi-cli` was tested with the following Supermicro motherboards and IPMI firmware versions:

| Motherboard manufacturer | Motherboard model | IPMI firmware version |
| ------------------------ | ----------------- | --------------------- |
| Supermicro               | X10DDW-i          | 3.24                  |
| Supermicro               | X10DRW-i          | 3.45                  |
| Supermicro               | X10DRW-i          | 3.58                  |
| Supermicro               | X10DRW-i          | 3.71                  |
| Supermicro               | X10DRW-i          | 3.86                  |
| Supermicro               | X10DRW-i          | 3.88                  |
| Supermicro               | X11SSH-LN4F       | 1.11                  |
| Supermicro               | X11SSH-LN4F       | 1.39                  |
| Supermicro               | X11SSH-LN4F       | 1.45                  |
| Supermicro               | X11SSH-LN4F       | 1.48                  |
| Supermicro               | X11SSH-LN4F       | 1.55                  |
| Supermicro               | X11SSH-LN4F       | 1.56                  |
| Supermicro               | X11SSH-LN4F       | 1.58                  |
| Supermicro               | X11SSH-LN4F       | 1.60                  |
| Supermicro               | X9SCL/X9SCM       | 1.64                  |
| Supermicro               | X9SCL/X9SCM       | 1.86                  |
| Supermicro               | X9SCL/X9SCM       | 2.38                  |
| Supermicro               | X9SCL/X9SCM       | 3.19                  |
| Supermicro               | X9SCL/X9SCM       | 3.38                  |
| Supermicro               | X9SCL/X9SCM       | 3.52                  |

## Build

### Requirements

- GO

### Building the binary

As simple as `script/build`. The generated binary will be found as `bin/[linux|macos|windows]/ipmi-cli`.

## Making a release

- In the master branch, Add an entry to CHANGELOG.md with the release date and changes from the previous version
- Update the version number in `softwareversion.go`
- Commit with a message, ex: "Update changelog, bump to vX.Y.Z"
- Run `script/release` to create a tag. This will also build the binaries to `/bin/[linux|macos|windows]/`
- Modify the release notes on Gitlab.

## Using pre-build binaries

Download the latest release for your operating system (ideally in a folder that is part of your path).

### Linux

The binary needs to be made executable with :

```bash
chmod +x ipmi-cli
```

Enjoy!

## Command-line usage

```
IPMI setup allows you to configure your IPMIs the easy way!

Usage:
  ipmi-cli [command]

Available Commands:
  acl         Set, display or update IPMI ACL rules
  config      Setup your ipmi-cli configuration file
  help        Help about any command
  reboot      Reboot your IPMI
  tls         Upload SSL certificate and key
  user        List, create, update or delete and IPMI user

Flags:
  -c, --config string          config file (default is $HOME/.ipmi-cli.yml)
  -h, --help                   help for ipmi-cli
      --host string            Host of the IPMI (Required)
      --loginpassword string   Password used to log into the IPMI (Required)
      --loginuser string       Username used to log into the IPMI (Required)
      --model string           Model of the IPMI (default "supermicro")
      --output string          Output format
                                 Possible values: [text, json] (default "text")
      --verbose                Verbose mode
  -v, --version                version for ipmi-cli

Use "ipmi-cli [command] --help" for more information about a command.
```

### Global optiflagsons

| Global options  |                   Description                    |
| --------------- | ------------------------------------------------ |
| `config` or `c` | Configuration file to use                        |
| `host`          | Specify target host                              |
| `model`         | Specify model of IPMI (default: "supermicro")    |
| `output`        | Preferred output format (default: "text")        |
| `loglevel`      | Log level to be printed (default: "info")        |
| `loginuser`     | Username for the IPMI login                      |
| `loginpassword` | Password for the IPMI login                      |
| `help`          | Displays this help menu                          |
| `version`       | Output version information only (default: false) |


### Commands and sub commands

| Command   | Sub command |                      Description                      |
| --------- | ----------- | ----------------------------------------------------- |
| `config`  |             | Setup your ipmi-cli configuration file                |
| `acl`     |             |                                                       |
|           | `list`      | List current ACL rules                                |
|           | `update`    | Update a single rule of ACL rule                      |
|           | `set`       | Wipe existing ACL rules and sets new ones from a file |
|           | `delete`    | Wipe an existing rule                                 |
|           | `deleteall` | Wipe all existing rules                               |
| `user`    |             |                                                       |
|           | `list`      | List all usernames and permissions                    |
|           | `create`    | Create a user                                         |
|           | `update`    | Update a user                                         |
|           | `delete`    | Delete a user                                         |
| `tls`     |             |                                                       |
|           | `update`    | Update TLS information and reboot IPMI                |
| `network` |             |                                                       |
|           | `list`      | List current IPv4 settings                            |
|           | `update`    | Provide a list of IPv4 settings to update             |
| `time`    |             |                                                       |
|           | `list`      | List current date and time settings                   |
|           | `update`    | Provide a list of date and time settings to update    |
| `reboot`  |             | Reboot the IPMI BMC                                   |

To view the help menu of any command or subcommand simply type the command/subcommand without any parameters or add `--help`.
There may be exceptions to this, as an example, the `reboot` command will not display any help menu. It will execute the command.

- `host`, `loginuser` and `loginpassword` are required for any commands unless your using a configuration file.
- `output` supports `JSON` and `text`. Default is human readable text output.
- `loglevel` has 7 level:
	- Trace
	- Debug
	- Info
	- Warn
	- Error
	- Fatal
	- Panic

## Commands details

### <a name="configuration"></a> Configuration (optional)

The `config` command will help you set up a configuration file. This configuration file can contain certain parameters that are usually manually entered through flags or arguments of a command. As an example, the required global flag `loginuser` and `loginpassword` are necessary in order to perform any operations on an IPMI. Therefore if it is not given by either a configuration file or flags, the program will give you an error. If it is defined through the configuration file, the program will use this value unless a flag is given. The flag will take priority and its value will be used.

USAGE:

```bash
./ipmi-cli config
```

If a configuration file is found in the default or specified location (through -c flag). You will be asked if you want the existing configuration file to be overwritten. Entering 'n' will abort the configuration. Entering 'y' will be followed by a few questions and your configuration file will be created.

The default location for configuration file is at `$HOME/.ipmi-cli.yml` for linux and `%HOMEDRIVE%%HOMEPATH%` for Windows.

### List the current ACL

The IPMI ACL is a list of IPv4 CIDR addresses. Each address has a policy field that is filled with either "ACCEPT" or "DROP". "ACCEPT" will allow access to the IPMI whilst "DROP" will forbid access. The rules follow the order in which they are applied meaning that if your first rule is "0.0.0.0/0 DROP", all traffic will be dropped, even if the following rules provide access.

USAGE:

```bash
./ipmi-cli --host=[host] --loginuser=[loginuser] --loginpassword=[loginpassword] acl list
```

JSON output :

```json
{
  "state": "enable",
  "rules": [
    {
      "priority": "1",
      "ipv4_cidr": "1.1.1.1",
      "policy": "ACCEPT"
    },
    {
      "priority": "2",
      "ipv4_cidr": "2.2.2.2/29",
      "policy": "ACCEPT"
    },
	... shortened output ...
  ]
}
```

Text output :

```
ACL State: enable
1  IPv4 CIDR: 1.1.1.1        Policy: ACCEPT
2  IPv4 CIDR: 2.2.2.2/29     Policy: ACCEPT
3  IPv4 CIDR: 3.3.3.3/24     Policy: ACCEPT
4  IPv4 CIDR: 4.4.4.4/24     Policy: ACCEPT
5  IPv4 CIDR: 5.5.5.5        Policy: ACCEPT
6  IPv4 CIDR: 6.6.6.0/26     Policy: ACCEPT
7  IPv4 CIDR: 7.7.7.0/24     Policy: ACCEPT
8  IPv4 CIDR: 8.8.8.8        Policy: ACCEPT
9  IPv4 CIDR: 9.9.9.9        Policy: ACCEPT
```

If you want to save an output of the command to a file, You can use the save flag.
This flag accepts a file location. To save to the current directory, you must enter in `--save=.`

Usage: `--save=path/to/location`

Depending on the output format of the command, the extension of the file will be different.
Ex:
  - JSON ouput will generate a file ending in `.json`
  - text output will generate a file ending in `.txt`

### <a name="acl_update"></a> Update the ACL

Allows to update a single rule.

USAGE:

```bash
./ipmi-cli --host=[host] --loginuser=[loginuser] --loginpassword=[loginpassword] acl update --ruleno=1 --rule=[ipv4/cidr] --policy=[ACCEPT or DROP]
```

Output:

Same as `acl list` after the operation has completed.

### <a name="acl_set"></a> Upload an ACL list

Deletes all rules and apply new ones from a JSON file provided.

USAGE:

```bash
./ipmi-cli --host=[host] --loginuser=[loginuser] --loginpassword=[loginpassword] acl set --aclfile=[Path_to_JSON_file]
```

Output:

Same as `acl list` after the operation has completed.

#### JSON File format

The following is an example of JSON formatted ACL that can be used with the `acl set` command.
Note that `acl set` uses the same format as the JSON data obtained by `--output=JSON acl list` command. If you setup an IPMI the way you want, you can simply obtain the JSON from it and apply the rules to multiple other IPMIs.

```json
{
	"rules": [{
		"priority": "1",
		"ipv4_cidr": "1.1.1.1",
		"policy": "ACCEPT"
	}, {
		"priority": "2",
		"ipv4_cidr": "2.2.2.2/29",
		"policy": "ACCEPT"
	}, {
		"priority": "3",
		"ipv4_cidr": "3.3.3.3/24",
		"policy": "ACCEPT"
	}, {
		"priority": "4",
		"ipv4_cidr": "4.4.4.4/24",
		"policy": "ACCEPT"
	}, {
		"priority": "5",
		"ipv4_cidr": "5.5.5.5",
		"policy": "ACCEPT"
	}, {
		"priority": "6",
		"ipv4_cidr": "6.6.6.0/26",
		"policy": "ACCEPT"
	}, {
		"priority": "7",
		"ipv4_cidr": "7.7.7.0/24",
		"policy": "ACCEPT"
	}, {
		"priority": "8",
		"ipv4_cidr": "8.8.8.8",
		"policy": "ACCEPT"
	}, {
		"priority": "9",
		"ipv4_cidr": "9.9.9.9",
		"policy": "ACCEPT"
	}]
}
```

### <a name="acl_delete"></a> Delete an ACL entry

Allows to delete a single rule.

USAGE:

```bash
./ipmi-cli --host=[host] --loginuser=[loginuser] --loginpassword=[loginpassword] acl delete --ruleno=[ruleno]
```

### <a name="acl_deleteall"></a> Flush the ACL

Delete all rules.

USAGE:

```bash
./ipmi-cli --host=[host] --loginuser=[loginuser] --loginpassword=[loginpassword] acl deleteall
```

### <a name="user_list"></a> List users

Provides a list of current users.

USAGE:

```bash
./ipmi-cli --host=[host] --loginuser=[loginuser] --loginpassword=[loginpassword] user list
```

JSON output:

```json
{"users":[{"name":"andre","privilege":"Administrator"}]}
```

Text output:

```text
Username: andre           Privilege: Administrator
Username: test            Privilege: Administrator
```

### <a name="user_create"></a> Create a user

Allows to create a user.

USAGE:

```bash
./ipmi-cli --host=[host] --loginuser=[loginuser] --loginpassword=[loginpassword] user create --username=[username] --password=[password] --permission=[permission]
```

Output:

```text
User added successfully
```

### <a name="update_user"></a> Update a user

Allows to update a single user.

USAGE:

```bash
./ipmi-cli --host=[host] --loginuser=[loginuser] --loginpassword=[loginpassword] user create --username=[username] --newusername=[new_username] --password=[password] --permission=[permission]
```

Output:

```text
User updated successfully
```

### <a name="user_delete"></a> Delete a user

Deletes a single user.

USAGE:

```bash
./ipmi-cli --host=[host] --loginuser=[loginuser] --loginpassword=[loginpassword] user create --username=[username]
```

Output:

```text
Successfully deleted user
```

### <a name="tls_update"></a> Upload the TLS certificate and key

USAGE:

```bash
./ipmi-cli --host=[host] --loginuser=[loginuser] --loginpassword=[loginpassword] tls update --key=[Path_to_file] --cert=[Path_to_file]
```

Example Output :

```text
Certificate and key uploaded!
Rebooting IPMI to apply changes...
```

## License

Copyright Intello Technologies Inc, 2016-2020. Licensed under the [MIT license](/LICENSE?raw=true).

## About Intello Technologies Inc.

Intello is sponsoring this project as we believe in open-source and the open-source community.

Intello Technologies was founded by experts from hotel & IT industry backgrounds in 2003. Based in Montreal, Canada, we're more than just a services company or a simple VAR: we develop our own in-house software which we integrate with existing industry-leading hardware and then support for years to come.

Our team has in-depth knowledge of software development combined with years of sales and field management experience in the hospitality and lodging industries. Our solutions have been deployed throughout the United States, Canada, Mexico, the Caribbean and even in Africa, and our helpdesk ensures our customers are supported around the clock. Recognized as industry leaders in our fields of expertise, our company goal is to provide great solutions and support.

Find out more on our web site at <https://www.intello.com>
