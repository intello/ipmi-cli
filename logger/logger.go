package logger

import (
	"os"

	"github.com/sirupsen/logrus"
)

var logger *logrus.Logger = logrus.New()

// Init initializes the needed for logging
func Init(flag string) {
	logger.Out = os.Stdout
	setLogLevel(flag)

	formatter := &logrus.TextFormatter{
		DisableTimestamp: true,
	}

	logger.SetFormatter(formatter)
}

// Log will log a string given at a provided level
func Log(text string, level logrus.Level) {
	switch level {
	case logrus.TraceLevel:
		logger.Trace(text)
	case logrus.DebugLevel:
		logger.Debug(text)
	case logrus.InfoLevel:
		logger.Info(text)
	case logrus.WarnLevel:
		logger.Warn(text)
	case logrus.ErrorLevel:
		logger.Error(text)
	case logrus.FatalLevel:
		logger.Fatal(text)
	case logrus.PanicLevel:
		logger.Panic(text)
	}
}

// setLogLevel sets log level from the CLI flag provided
// If the value passed in the flag doesn't exist,
// the default level will be applied.
func setLogLevel(flag string) {
	parsedLevel, err := logrus.ParseLevel(flag)

	if err != nil {

		// If it can't parse the level, automatically do error level
		logger.SetLevel(logrus.ErrorLevel)
	} else {
		logger.SetLevel(parsedLevel)
	}
}
