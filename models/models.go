package models

import (
	"fmt"
	"ipmi-cli/interfaces"
	"ipmi-cli/models/asrock"
	"ipmi-cli/models/supermicro"
)

// Handler uses the model flag and dispatches it to the proper module
func Handler(options *interfaces.CLIOptions) (string, error) {
	switch options.Model {
	case "supermicro":
		return supermicro.Init(options)
	case "asrock":
		return asrock.Init(options)
	}

	return "", fmt.Errorf("Model: '%v' not found", options.Model)
}
