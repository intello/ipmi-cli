package supermicro

import (
	"encoding/xml"
	"ipmi-cli/interfaces"
	"ipmi-cli/logger"
	"ipmi-cli/models/supermicro/version"
	"ipmi-cli/web"
	"net/url"
	"strings"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

var sid string

// The following structs are required for XML parsing through the encoding/xml package
type ipmi struct {
	XMLName     xml.Name    `xml:"IPMI"`
	Genericinfo genericinfo `xml:"GENERIC_INFO"`
}

type genericinfo struct {
	Generic generic `xml:"GENERIC"`
}

type generic struct {
	IpmiFWTag     string `xml:"IPMIFW_TAG,attr"`
	IpmiFWVersion string `xml:"IPMIFW_VERSION,attr"`
}

// Init will init the supermicro models
func Init(options *interfaces.CLIOptions) (string, error) {
	var result string = ""

	webOptions := web.ClientOptions{
		BaseURL:               options.Host,
		APIPath:               "cgi",
		Cookies:               make(map[string]string),
		ContentType:           "application/x-www-form-urlencoded",
		CertificateValidation: true,
	}

	web.Init(webOptions)

	// Login and get SID from cookies
	sessionID := getSessionID(options.Host, options.LoginUser, options.LoginPassword)

	if sessionID == "" {
		return result, errors.New("Unable to log into the IPMI, please verify credentials")
	}

	web.AddCookie("SID", sessionID)

	return version.Handle(options)
}

func getSessionID(host string, user string, password string) string {

	// Prepare data to send
	data := url.Values{}
	data.Set("name", user)
	data.Set("pwd", password)

	resp, err := web.Post("login.cgi", strings.NewReader(data.Encode()))
	if err != nil {
		logger.Log(errors.Wrap(err, "Login page of IPMI unreachable").Error(), logrus.InfoLevel)
	}

	sidValue := ""

	// Iterate through cookies and try to find SID
	for _, cookie := range resp.Cookies {
		if cookie.Value != "" && cookie.Name == "SID" {
			sidValue = cookie.Value
			break
		}
	}

	return sidValue
}
