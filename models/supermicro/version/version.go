package version

import (
	"errors"
	"fmt"
	"ipmi-cli/errhandler"
	"ipmi-cli/interfaces"
	"ipmi-cli/logger"
	"ipmi-cli/models/supermicro/version/common"

	"github.com/sirupsen/logrus"

	"github.com/fatih/color"
)

// Handle will proceed with the correct methods for a given version
func Handle(options *interfaces.CLIOptions) (string, error) {
	var result string
	var err error = nil

	v := getVersion()

	if v == "unknown" {
		err = fmt.Errorf(color.RedString("IPMI version unknown, please contact developpers"))
		errhandler.ExitIfErr(err)
	}

	options.Version = v
	result, err = handleCommand(options)

	return result, err
}

func getVersion() string {
	if common.CheckIfV1() {
		return "v1"
	} else if common.CheckIfV2() {
		return "v2"
	}

	return "unknown"
}

// handleCommand will handle the command passed
// from the CLI according to this IPMI version's required parameters.
func handleCommand(options *interfaces.CLIOptions) (string, error) {
	var result string
	var err error

	// Maybe not hardcore this?
	if options.Version != "v1" && options.Version != "v2" {
		return "", errors.New("Unkwown version")
	}

	switch options.Command {
	case "acl":
		switch options.Subcommand {
		case "list":
			return common.ListACL(options)
		case "set":
			return common.SetACL(options), nil
		case "update":
			return common.UpdateACL(options)
		case "delete":
			return common.DeleteRule(options.RuleNumber, options.Version), nil
		case "deleteall":
			return common.DeleteAllRules(options), nil
		}
	case "user":
		switch options.Subcommand {
		case "list":
			return common.ListUser(options)
		case "create":
			return common.CreateUser(options)
		case "delete":
			return common.DeleteUser(options)
		case "update":
			return common.UpdateUser(options)
		}
	case "tls":
		switch options.Subcommand {
		case "update":
			result, err := common.UpdateTLS(options)
			if common.VerifyCert(options) {
				color.Green("Certificate and key uploaded!")
				common.RebootIPMI(options)
			} else {
				logger.Log(color.RedString("Failed to upload certificate and key files"), logrus.FatalLevel)
			}

			return result, err
		}
	case "time":
		switch options.Subcommand {
		case "list":
			return common.ListTime(options)
		case "update":
			return common.UpdateTime(options)
		}
	case "network":
		switch options.Subcommand {
		case "list":
			return common.ListNetwork(options)
		case "update":
			return common.UpdateNetwork(options)
		}
	}

	return result, err
}
