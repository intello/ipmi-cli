package common

import (
	"ipmi-cli/interfaces"
	"ipmi-cli/models/supermicro/utils"
	"ipmi-cli/web"
	"net/url"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// ListNetwork will give you a list of network settings
func ListNetwork(options *interfaces.CLIOptions) (string, error) {

	// Prepare data to send
	data := url.Values{}
	if options.Version == "v1" {
		data.Set("CONFIG_INFO.XML", "(0,0)")
		data.Set("time_stamp", time.Now().String())
	} else if options.Version == "v2" {
		data.Set("op", "CONFIG_INFO.XML")
		data.Set("r", "(0,0)")
	}

	resp, err := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))
	if err != nil {
		return "", errors.Wrap(err, "Error getting network settings")
	}

	return utils.ExtractNetworkFromXML(resp.Body)
}
