package common

import (
	"fmt"
	"ipmi-cli/interfaces"
	"ipmi-cli/models/supermicro/utils"
	"ipmi-cli/web"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// UpdateUser will update a user
func UpdateUser(options *interfaces.CLIOptions) (string, error) {
	data := url.Values{}

	if options.Version == "v1" {
		data.Set("CONFIG_INFO.XML", "(0,0)")
		data.Set("time_stamp", time.Now().String())
	} else if options.Version == "v2" {
		data.Set("op", "CONFIG_INFO.XML")
		data.Set("r", "(0,0)")
	}

	resp, _ := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))

	foundUser := utils.CheckIfUserExists(resp.Body, options.Username)
	foundNewUser := utils.CheckIfUserExists(resp.Body, options.NewUsername)

	if len(options.Password) < 8 {
		return "", errors.New("Your password must have 8 or more characters")
	}

	// Check if the current username doesn't exist
	if foundUser.Name == "" {
		return "", fmt.Errorf("User '%v' not found", options.Username)
	}

	// Check if the current provided already exists
	if foundNewUser.Name != "" {
		return "", fmt.Errorf("User '%v' already exists", options.NewUsername)
	}

	data = url.Values{}

	// If the new username isn't empty and it doesn't already exist, we want to update it.
	if options.NewUsername != "" {
		data.Set("username", options.NewUsername)
	} else {

		// If the username is empty, use the old username
		data.Set("username", options.Username)
	}

	// No errors so far, we can continue safely
	id, _ := strconv.Atoi(foundUser.UserID)
	foundUser.UserID = strconv.Itoa(id - 1)

	data.Set("original_username", foundUser.UserID)
	data.Set("password", options.Password)
	data.Set("new_privilege", utils.DeterminePrivilegeNumber(options.Permission))

	if options.Version == "v1" {
		web.Post("config_user.cgi", strings.NewReader(data.Encode()))
	} else if options.Version == "v2" {
		data.Set("op", "config_user")
		web.Post("op.cgi", strings.NewReader(data.Encode()))
	}

	return "User updated successfully", nil
}
