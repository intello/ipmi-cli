package common

import (
	"fmt"
	"ipmi-cli/interfaces"
	"ipmi-cli/web"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// UpdateACL updates a single ACL rule
func UpdateACL(options *interfaces.CLIOptions) (string, error) {

	// First, we need to verify if the rule already exists
	if ruleAlreadyExists(options) {
		return "", fmt.Errorf("Rule for %v already exists", options.Rule)
	}

	data := url.Values{}
	data.Set("op", "ip_ctrl")
	data.Set("mode", determineModeForUpdate(options))
	data.Set("ruleno", strconv.Itoa(options.RuleNumber))
	data.Set("ipinfo", options.Rule)
	data.Set("policy", strings.ToUpper(options.Policy))

	if options.Version == "v1" {
		data.Set("time_stamp", time.Now().String())
	}

	_, err := web.Post("op.cgi", strings.NewReader(data.Encode()))
	if err != nil {
		return "", fmt.Errorf("Error trying to add rule for %v", options.Rule)
	}

	return ListACL(options)
}
