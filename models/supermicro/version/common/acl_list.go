package common

import (
	"ipmi-cli/interfaces"
	"ipmi-cli/models/supermicro/utils"
	"ipmi-cli/web"
	"net/url"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// ListACL will lists the access control list from the API
func ListACL(options *interfaces.CLIOptions) (string, error) {

	// Prepare data to send
	data := url.Values{}
	if options.Version == "v1" {
		data.Set("IP_ACCESS_CTRL.XML", "(0,0)")
		data.Set("time_stamp", time.Now().String())
	} else if options.Version == "v2" {
		data.Set("op", "IP_ACCESS_CTRL.XML")
		data.Set("r", "(0,0)")
	}

	resp, err := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))
	if err != nil {
		return "", errors.Wrap(err, "Error getting ACL")
	}

	return utils.ExtractACLFromXML(resp.Body, options.Output)
}
