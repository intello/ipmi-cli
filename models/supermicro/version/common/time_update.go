package common

import (
	"ipmi-cli/interfaces"
	"ipmi-cli/logger"
	"ipmi-cli/models/supermicro/utils"
	"ipmi-cli/web"
	"net/url"
	"regexp"
	"strconv"
	"strings"

	"github.com/sirupsen/logrus"
)

// UpdateTime will update a list of time settings
func UpdateTime(options *interfaces.CLIOptions) (string, error) {
	var err error

	var timezone = options.Timezone
	var dstEn = options.DST
	var ntpEn = options.NTP
	var ntpServ1 = options.NTPServ1
	var ntpServ2 = options.NTPServ2
	var year string
	var month string
	var day string
	var hour string
	var minute string
	var second string

	values, _ := utils.GetTimeValuesFromIPMI(options)

	// Parse time and date
	year, month, day = parseDate(options.Date)
	hour, minute, second = parseTime(options.Time)

	if timezone == "" {
		timezone = values.Timezone
	} else {
		timezone = calculateTimeZone(timezone)
	}

	if year == "" || month == "" || day == "" {
		logger.Log("Date not provided or invalid format... Using IPMI date", logrus.InfoLevel)
		year = values.Year
		month = values.Month
		day = values.Day
	}

	if hour == "" || minute == "" || second == "" {
		logger.Log("Time not provided or invalid format... Using IPMI time", logrus.InfoLevel)
		hour = values.Hour
		minute = values.Minute
		second = values.Second
	}

	if timezone == "" {
		timezone = values.Timezone
	}

	// Grab from values or convert it to number if it's from parameters.
	if dstEn == "" {
		dstEn = values.DST
	} else if dstEn == "true" {
		dstEn = "1"
	} else if dstEn == "false" {
		dstEn = "0"
	}

	if ntpEn == "" {
		ntpEn = values.NTP
	} else if ntpEn == "true" {
		ntpEn = "on"
	} else if ntpEn == "false" {
		ntpEn = "off"
	}

	if ntpServ1 == "" {
		ntpServ1 = values.NTP_Serv_1
	}

	if ntpServ2 == "" {
		ntpServ2 = values.NTP_Serv_2
	}

	// Prepare data to send
	data := url.Values{}
	data.Set("timezone", timezone)
	data.Set("dst_en", dstEn)
	data.Set("ntp", ntpEn)
	data.Set("ntp_server_pri", ntpServ1)
	data.Set("ntp_server_2nd", ntpServ2)
	data.Set("year", year)
	data.Set("month", month)
	data.Set("day", day)
	data.Set("hour", hour)
	data.Set("min", minute)
	data.Set("sec", second)

	if options.Version == "v2" {
		data.Set("op", "config_date_time")
		_, err = web.Post("op.cgi", strings.NewReader(data.Encode()))
	} else {
		_, err = web.Post("config_date_time.cgi", strings.NewReader(data.Encode()))
	}

	if err != nil {
		return "", err
	}

	return ListTime(options)
}

// Returns 3 strings
// year, month and day in that order.
func parseDate(date string) (string, string, string) {
	var year, month, day string

	dateReg := regexp.MustCompile(`^([12]\d{3})-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01])$`)
	dateMatch := dateReg.FindStringSubmatch(date)
	if len(dateMatch) >= 3 {
		year = dateMatch[1]
		month = dateMatch[2]
		day = dateMatch[3]
	}

	return year, month, day
}

// Returns 3 strings
// hour, minute, second in that order.
func parseTime(time string) (string, string, string) {
	var hour, minute, second string

	timeReg := regexp.MustCompile(`^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$`)
	timeMatch := timeReg.FindStringSubmatch(time)
	if len(timeMatch) >= 3 {
		hour = timeMatch[1]
		minute = timeMatch[2]
		second = timeMatch[3]
	}

	return hour, minute, second
}

// IPMI requires the timezone difference from 0 in seconds.
// Ex: UTC+1 = 3600, UTC+2 = 7200, UTC-1 = -3600
func calculateTimeZone(timezone string) string {
	z, _ := strconv.Atoi(timezone)
	return strconv.Itoa(z * 3600)
}
