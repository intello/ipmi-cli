package common

import (
	"ipmi-cli/interfaces"
	"ipmi-cli/models/supermicro/utils"
	"ipmi-cli/web"
	"net/url"
	"strings"
	"time"
)

// ListUser will list the users
func ListUser(options *interfaces.CLIOptions) (string, error) {
	data := url.Values{}

	if options.Version == "v1" {
		data.Set("CONFIG_INFO.XML", "(0,0)")
		data.Set("time_stamp", time.Now().String())
	} else if options.Version == "v2" {
		data.Set("op", "CONFIG_INFO.XML")
		data.Set("r", "(0,0)")
	}

	resp, _ := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))

	return utils.ExtractUsersFromXML(resp.Body, options.Output)
}
