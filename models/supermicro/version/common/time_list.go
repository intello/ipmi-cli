package common

import (
	"ipmi-cli/interfaces"
	"ipmi-cli/models/supermicro/utils"
	"ipmi-cli/web"
	"net/url"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// ListTime will give you a list of time settings
func ListTime(options *interfaces.CLIOptions) (string, error) {

	// Prepare data to send
	data := url.Values{}
	if options.Version == "v1" {
		data.Set("GET_DATE_TIME.XML", "(0,0)")
		data.Set("time_stamp", time.Now().String())
	} else if options.Version == "v2" {
		data.Set("op", "GET_DATE_TIME.XML")
		data.Set("r", "(0,0)")
	}

	resp, err := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))
	if err != nil {
		return "", errors.Wrap(err, "Error getting time settings")
	}

	return utils.ExtractTimeFromXML(resp.Body)
}
