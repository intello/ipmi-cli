package common

import (
	"fmt"
	"ipmi-cli/interfaces"
	"ipmi-cli/models/supermicro/utils"
	"ipmi-cli/web"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// DeleteUser will delete a user
func DeleteUser(options *interfaces.CLIOptions) (string, error) {
	data := url.Values{}

	if options.Version == "v1" {
		data.Set("CONFIG_INFO.XML", "(0,0)")
		data.Set("time_stamp", time.Now().String())
	} else if options.Version == "v2" {
		data.Set("op", "CONFIG_INFO.XML")
		data.Set("r", "(0,0)")
	}

	resp, _ := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))
	foundUser := utils.CheckIfUserExists(resp.Body, options.Username)

	if foundUser.UserID != "" {
		// When you delete a user, the ID is -1 from what it is on the webUI
		id, _ := strconv.Atoi(foundUser.UserID)
		id--

		var resp web.Response

		if options.Version == "v1" {
			resp, _ = web.Post("config_user.cgi", strings.NewReader(fmt.Sprintf("username=&original_username=%v", id)))
		} else if options.Version == "v2" {
			data := url.Values{}
			data.Set("username", "")
			data.Set("original_username", strconv.Itoa(id))
			data.Set("op", "config_user")
			resp, _ = web.Post("op.cgi", strings.NewReader(data.Encode()))
		}

		if resp.Body == "\nok\n" {
			return "Successfully deleted user", nil
		}
	}

	return "", fmt.Errorf("%v not found", options.Username)
}
