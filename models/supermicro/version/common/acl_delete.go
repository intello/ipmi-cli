package common

import (
	"ipmi-cli/web"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// DeleteRule will delete a single rule from the ACL
func DeleteRule(ruleNumber int, version string) string {

	// Prepare data to send
	data := url.Values{}
	data.Set("op", "ip_ctrl")
	data.Set("mode", "delete")
	data.Set("ruleno", strconv.Itoa(ruleNumber))

	if version == "v1" {
		data.Set("time_stamp", time.Now().String())
	}

	resp, _ := web.Post("op.cgi", strings.NewReader(data.Encode()))

	if resp.Body == "\nok\n" {
		return "Successfully deleted rule"
	}

	return "Something went wrong"
}
