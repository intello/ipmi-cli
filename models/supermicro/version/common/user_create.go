package common

import (
	"fmt"
	"ipmi-cli/interfaces"
	"ipmi-cli/models/supermicro/utils"
	"ipmi-cli/web"
	"net/url"
	"strconv"
	"strings"
)

// CreateUser will create a new user
func CreateUser(options *interfaces.CLIOptions) (string, error) {
	var err error = nil
	numberUsers := countNumberUsers(options.Version)

	if userAlreadyExists(options.Username, options.Version) {
		err = fmt.Errorf("%v user already exists", options.Username)
	} else if numberUsers == 10 {
		err = fmt.Errorf("You cannot have more than 10 users")
	} else if len(options.Password) < 8 {
		err = fmt.Errorf("Your password must have 8 or more characters")
	} else {
		data := url.Values{}
		data.Set("username", options.Username)

		// This is the user ID
		data.Set("original_username", strconv.Itoa(numberUsers))
		data.Set("password", options.Password)
		data.Set("new_privilege", utils.DeterminePrivilegeNumber(options.Permission))

		if options.Version == "v1" {
			web.Post("config_user.cgi", strings.NewReader(data.Encode()))
		} else if options.Version == "v2" {
			data.Set("op", "config_user")
			web.Post("op.cgi", strings.NewReader(data.Encode()))
		}

		return "User added successfully", err
	}

	return "", err
}
