package common

import (
	"bytes"
	"fmt"
	"io"
	"io/ioutil"
	"ipmi-cli/errhandler"
	"ipmi-cli/interfaces"
	"ipmi-cli/web"
	"mime/multipart"
	"net/http"
	"net/textproto"
	"os"

	"github.com/fatih/color"
	"github.com/pkg/errors"
)

// UpdateTLS will update the TLS config
func UpdateTLS(options *interfaces.CLIOptions) (string, error) {
	var err error = nil

	// Verify if files exist
	keyExist := fileExists(options.Key)
	certExist := fileExists(options.Cert)
	errMessage := color.RedString("The following files could not be found :\n")

	if !keyExist {
		errMessage += color.RedString("  %v\n", options.Key)
		err = fmt.Errorf(errMessage)
	}

	if !certExist {
		errMessage += color.RedString("  %v\n", options.Cert)
		err = fmt.Errorf(errMessage)
	}

	errhandler.ExitIfErr(err)

	// Check if files given exist
	if _, err := os.Stat(options.Cert); err != nil {
		err = fmt.Errorf("Unable to find certificate file: %v", options.Cert)
	}

	if _, err := os.Stat(options.Key); err != nil {
		err = fmt.Errorf("Unable to find key file: %v", options.Key)
		errhandler.ExitIfErr(err)
	}

	// This array contains the path of each file to upload
	mediaFiles := []string{options.Cert, options.Key}

	// Create buffer and multipart writer
	b := &bytes.Buffer{}
	writer := multipart.NewWriter(b)

	if options.Version == "v1" {

		// For each file we need to upload, create a part and add it to the writer
		for _, mediaFilename := range mediaFiles {
			mediaData, _ := ioutil.ReadFile(mediaFilename)
			mediaHeader := textproto.MIMEHeader{}
			mediaHeader.Set("Content-Disposition", fmt.Sprintf("form-data; filename=\"%v\".", mediaFilename))
			mediaHeader.Set("Content-Type", "application/octet-stream")

			mediaPart, _ := writer.CreatePart(mediaHeader)
			io.Copy(mediaPart, bytes.NewReader(mediaData))
		}
	} else if options.Version == "v2" {

		// Create a part for the cert and key file and add it to the writer
		mediaData, _ := ioutil.ReadFile(mediaFiles[0])
		mediaHeader := textproto.MIMEHeader{}
		mediaHeader.Set("Content-Disposition", fmt.Sprintf("form-data; name=\"cert_file\"; filename=\"%v\".", mediaFiles[0]))
		mediaHeader.Set("Content-Type", "application/octet-stream")

		mediaPart, _ := writer.CreatePart(mediaHeader)
		io.Copy(mediaPart, bytes.NewReader(mediaData))

		mediaData, _ = ioutil.ReadFile(mediaFiles[1])
		mediaHeader = textproto.MIMEHeader{}
		mediaHeader.Set("Content-Disposition", fmt.Sprintf("form-data; name=\"key_file\"; filename=\"%v\".", mediaFiles[1]))
		mediaHeader.Set("Content-Type", "application/octet-stream")

		mediaPart, _ = writer.CreatePart(mediaHeader)
		io.Copy(mediaPart, bytes.NewReader(mediaData))

		if web.GetCSRFToken() != "" {
			mediaHeader = textproto.MIMEHeader{}
			mediaHeader.Set("Content-Disposition", fmt.Sprint("form-data; name=\"CSRF_TOKEN\""))

			mediaPart, _ = writer.CreatePart(mediaHeader)
			io.Copy(mediaPart, bytes.NewReader([]byte(web.GetCSRFToken())))
		}
	}

	// Close the multipart writer
	writer.Close()

	// Change content-type param to multipart/related with the writers boundary
	web.ChangeContentType(fmt.Sprintf("multipart/related; boundary=%s", writer.Boundary()))

	// Send files to the IPMI
	resp, err := web.Post("upload_ssl.cgi", bytes.NewReader(b.Bytes()))
	if err != nil {
		return "", errors.New("Problem uploading SSL certificate and/or key")
	}

	if resp.HTMLCode != http.StatusOK {
		return "", fmt.Errorf("Request failed with response code: %d", resp.HTMLCode)
	}

	return "Successfully updated SSL configuration.\nYour IPMI will be back online shortly!\n", nil
}
