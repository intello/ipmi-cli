package common

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"ipmi-cli/errhandler"
	"ipmi-cli/interfaces"
	"ipmi-cli/models/supermicro/utils"
	"os"
	"strconv"
)

// SetACL will set the ACL rules from a JSON file
func SetACL(options *interfaces.CLIOptions) string {
	var result string
	var rules *utils.ACLToJSON

	jsonFile, err := os.Open(options.ACLFile)
	if err != nil {
		// Prettier error message
		err = fmt.Errorf("Unable to find ACL file: \"%v\"", options.ACLFile)
		errhandler.ExitIfErr(err)
	}

	byteValue, _ := ioutil.ReadAll(jsonFile)
	err = json.Unmarshal(byteValue, &rules)
	if err != nil {
		// Prettier error message
		err = fmt.Errorf("Invalid JSON format in file: \"%v\"", options.ACLFile)
		errhandler.ExitIfErr(err)
	}

	fmt.Println("Deleting all rules...")
	DeleteAllRules(options)

	fmt.Println("Adding ACL rules from provided file...")
	for _, value := range rules.Rules {
		options.RuleNumber, _ = strconv.Atoi(value.Priority)
		options.Rule = value.IP
		options.Policy = value.Policy

		UpdateACL(options)
		fmt.Printf("  %s - %s added\n", value.IP, value.Policy)
	}

	fmt.Println("")
	result, _ = ListACL(options)

	return result
}
