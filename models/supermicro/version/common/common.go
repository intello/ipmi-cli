package common

import (
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"ipmi-cli/htmlparser"
	"ipmi-cli/interfaces"
	"ipmi-cli/models/supermicro/utils"
	"ipmi-cli/web"
	"net/url"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/fatih/color"
)

// CheckIfV1 will check if the IPMI is of type "v1"
func CheckIfV1() bool {
	data := url.Values{}
	data.Set("IP_ACCESS_CTRL.XML", "(0,0)")
	data.Set("time_stamp", time.Now().String())

	result := sendDataToIPMI(data)

	return result != ""
}

// CheckIfV2 will check if the IPMI is of type "v2"
func CheckIfV2() bool {
	data := url.Values{}
	data.Set("op", "IP_ACCESS_CTRL.XML")
	data.Set("r", "(0,0)")

	result := sendDataToIPMI(data)

	return result != ""
}

func sendDataToIPMI(data url.Values) string {
	resp, _ := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))

	var validation *ipmi
	xml.Unmarshal([]byte(resp.Body), &validation)

	if validation.XMLName.Local == "" {
		// To support IPMI firmwares 1.60+
		CSRFToken := getCSRFToken("config_ip_ctrl")

		if CSRFToken != "" {
			web.ChangeCSRFToken(CSRFToken)

			var validation *ipmi
			xml.Unmarshal([]byte(resp.Body), &validation)
		}
	}

	return validation.XMLName.Local
}

func getCSRFToken(page string) string {
	resp, _ := web.Get("url_redirect.cgi?url_name=" + page)
	reader := strings.NewReader(resp.Body)
	startMatch := htmlparser.NewSkipTillReader(reader, []byte("<script>SmcCsrfInsert"))
	content := htmlparser.NewReadTillReader(startMatch, []byte("</script>"))
	bs, _ := ioutil.ReadAll(content)

	r := regexp.MustCompile(`\<script\>SmcCsrfInsert \(\"CSRF_TOKEN\", \"(.*)\"\);\</script\>`)
	token := r.FindStringSubmatch(string(bs))

	// If no token is found, return empty string
	if len(token) == 0 {
		return ""
	}

	return token[1]
}

func ruleAlreadyExists(options *interfaces.CLIOptions) bool {

	// Prepare data to send
	data := url.Values{}

	if options.Version == "v1" {
		data.Set("IP_ACCESS_CTRL.XML", "(0,0)")
		data.Set("time_stamp", time.Now().String())
	} else if options.Version == "v2" {
		data.Set("op", "IP_ACCESS_CTRL.XML")
		data.Set("r", "(0,0)")
	}

	resp, _ := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))

	return utils.CheckIfRuleExists(resp.Body, options.Rule)
}

// determineModeForUpdate determines if we need to add or modify a rule
func determineModeForUpdate(options *interfaces.CLIOptions) string {

	// Prepare data to send
	data := url.Values{}

	if options.Version == "v1" {
		data.Set("IP_ACCESS_CTRL.XML", "(0,0)")
		data.Set("time_stamp", time.Now().String())
	} else if options.Version == "v2" {
		data.Set("op", "IP_ACCESS_CTRL.XML")
		data.Set("r", "(0,0)")
	}

	resp, _ := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))

	return utils.ExtractACLRuleNoFromXML(resp.Body, options.RuleNumber)
}

func userAlreadyExists(user string, version string) bool {
	data := url.Values{}

	if version == "v1" {
		data.Set("CONFIG_INFO.XML", "(0,0)")
		data.Set("time_stamp", time.Now().String())
	} else if version == "v2" {
		data.Set("op", "CONFIG_INFO.XML")
		data.Set("r", "(0,0)")
	}

	resp, _ := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))
	foundUser := utils.CheckIfUserExists(resp.Body, user)

	return foundUser.Name != ""
}

func countNumberUsers(version string) int {
	data := url.Values{}

	if version == "v1" {
		data.Set("CONFIG_INFO.XML", "(0,0)")
		data.Set("time_stamp", time.Now().String())
	} else if version == "v2" {
		data.Set("op", "CONFIG_INFO.XML")
		data.Set("r", "(0,0)")
	}

	resp, _ := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))

	return utils.CountNumberUsersFromXML(resp.Body)
}

// fileExists checks if a file exists and is not a directory before we
// try using it to prevent further errors.
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

// VerifyCert asks the IPMI to verify the currently loaded certs.
// This should be called after uploading certificate and key files.
func VerifyCert(options *interfaces.CLIOptions) bool {
	data := url.Values{}

	if options.Version == "v1" {
		data.Set("SSL_VALIDATE.XML", "(0,0)")
		data.Set("time_stamp", time.Now().String())
	} else if options.Version == "v2" {
		data.Set("op", "SSL_VALIDATE.XML")
		data.Set("r", "(0,0)")
	}

	resp, _ := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))

	version, _ := strconv.Atoi(strings.TrimPrefix(options.Version, "v"))
	return utils.ProcessCertValidationResult(resp.Body, version)
}

// RebootIPMI will reboot the IPMI
func RebootIPMI(options *interfaces.CLIOptions) {
	var resp web.Response
	data := url.Values{}

	fmt.Println("Rebooting IPMI to apply changes...")

	if options.Version == "v1" {
		data.Set("time_stamp", time.Now().String())
		resp, _ = web.Post("BMCReset.cgi", strings.NewReader(data.Encode()))
	} else if options.Version == "v2" {
		data.Set("op", "main_bmcreset")
		resp, _ = web.Post("op.cgi", strings.NewReader(data.Encode()))
	}

	var validation *ipmi
	xml.Unmarshal([]byte(resp.Body), &validation)

	if validation.BMCRESET.STATE.CODE != "OK" {
		color.Red("IPMI reboot failed")
	}
}
