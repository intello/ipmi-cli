package common

import (
	"fmt"
	"ipmi-cli/interfaces"
	"ipmi-cli/models/supermicro/utils"
	"ipmi-cli/web"
	"net"
	"net/url"
	"strconv"
	"strings"
)

// UpdateNetwork will update a list of network settings
func UpdateNetwork(options *interfaces.CLIOptions) (string, error) {
	var err error

	values, _ := utils.GetNetworkValuesFromIPMI(options)
	valuesIPv6, _ := utils.GetNetworkIPv6ValuesFromIPMI(options)

	var dns = options.DNS
	var dhcpEnabled, _ = stringIntToBool(values.LAN.DHCPEn)
	var vlanID = values.LAN.VLAN_ID
	var vlanEnabled = values.LAN.VLAN_ID

	if vlanEnabled == "0001" || vlanEnabled == "0000" {
		vlanEnabled = "off"
	} else {
		vlanEnabled = "on"
	}

	if dns == "" || !validIP(dns) {
		dns = values.DNS.DNSServer
	}

	// Prepare data to send
	data := url.Values{}
	data.Set("dns_server", dns)
	data.Set("bmcmask", values.LAN.BMCMask)
	data.Set("gatewayip", values.LAN.GatewayIP)
	data.Set("en_dhcp", boolToOnOff(dhcpEnabled))
	data.Set("en_vlan", vlanEnabled)
	data.Set("vlanID", vlanID)
	data.Set("rmcpport", strconv.Itoa(int(hex2dec(values.LAN.RMCP_PORT))))
	data.Set("bmcipv6_dns_server", valuesIPv6.DNSServer.IP)
	data.Set("lan_interface", values.LanInterface.Interface)
	data.Set("link", values.MIILink.MIILinkConf)
	data.Set("hostname", values.Hostname.Name)
	// The settings below don't matter since we won't change anything... but the IPMI requires them anyway
	data.Set("bmcipv6_addr", "")
	data.Set("bmcipv6_opt", "add")
	data.Set("bmcipv6_autoconf", "on")
	data.Set("dhcpv6_mode", "stateless")

	if options.Version == "v2" {
		data.Set("op", "config_lan")
		_, err = web.Post("op.cgi", strings.NewReader(data.Encode()))
	} else {
		_, err = web.Post("config_lan.cgi", strings.NewReader(data.Encode()))
	}

	if err != nil {
		return "", err
	}

	return ListNetwork(options)
}

func validIP(ip string) bool {
	return net.ParseIP(ip) != nil
}

func stringIntToBool(val string) (bool, error) {
	if val == "0" {
		return false, nil
	} else if val == "1" {
		return true, nil
	}

	return false, fmt.Errorf("Invalid argument: %v", val)
}

func boolToOnOff(val bool) string {
	if val {
		return "on"
	}

	return "off"
}

func hex2dec(hex string) int64 {
	dec, _ := strconv.ParseInt("0x"+hex, 0, 16)
	return dec
}
