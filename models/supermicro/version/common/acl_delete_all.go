package common

import (
	"fmt"
	"ipmi-cli/interfaces"
	"ipmi-cli/models/supermicro/utils"
	"ipmi-cli/web"
	"net/url"
	"strconv"
	"strings"
	"time"
)

// DeleteAllRules will delete all rules from ACL
func DeleteAllRules(options *interfaces.CLIOptions) string {
	data := url.Values{}

	if options.Version == "v1" {
		data.Set("IP_ACCESS_CTRL.XML", "(0,0)")
		data.Set("time_stamp", time.Now().String())
	} else if options.Version == "v2" {
		data.Set("op", "IP_ACCESS_CTRL.XML")
		data.Set("r", "(0,0)")
	}

	// Find the DROP rules.
	resp, _ := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))
	rules := utils.GetACLRulesStruct(resp.Body)

	// Find the drop rules
	dropRules := findDropRules(rules)

	// Delete the drop rules
	if len(dropRules) > 0 {
		deleteDropRules(dropRules, options)
	} else {
		fmt.Println("Found no DROP Rules, let's move on!")
	}

	// Get the rules once again, there should be no more DROP rules remaining.
	resp, _ = web.Post("ipmi.cgi", strings.NewReader(data.Encode()))
	noRules := utils.CountNumberRules(resp.Body)
	if noRules > 0 {
		fmt.Printf("Found %d other rules, deleting them...\nThis may take a moment.\n\n", noRules)

		data = url.Values{}

		if options.Version == "v1" {
			data.Set("op", "ip_ctrl")
			data.Set("mode", "delete")
			data.Set("ruleno", "1")
			data.Set("time_stamp", time.Now().String())
		} else if options.Version == "v2" {
			data.Set("op", "ip_ctrl")
			data.Set("mode", "delete")
			data.Set("ruleno", "1")
		}

		for i := 0; i < noRules; i++ {
			web.Post("op.cgi", strings.NewReader(data.Encode()))
		}

		return "Deleted all ACL rules"
	}

	return "No other rules were found"
}

func findDropRules(rules *utils.Ipmi) []int {
	var dropRuleNos []int

	fmt.Println("Finding the 'DROP' rules to make sure we don't lock ourselves out...")

	for _, rule := range rules.IPAccessControl.FwRule {
		if rule.Policy == "DROP" {
			ruleno, _ := strconv.Atoi(rule.Priority)
			dropRuleNos = append(dropRuleNos, ruleno)
		}
	}

	return dropRuleNos
}

func deleteDropRules(ruleNos []int, options *interfaces.CLIOptions) {
	fmt.Printf("Found %d rules, deleting them...\n", len(ruleNos))

	// Let's first reverse the array, so we don't have to deal with changing rule priorities.
	// Otherwise, if I have rule 9 and 10 to delete and I delete rule 9, rule 10 will become rule 9.
	ruleNos = reverse(ruleNos)

	// Then start deleting them
	for _, i := range ruleNos {
		DeleteRule(i, options.Version)
	}
}

func reverse(numbers []int) []int {
	for i := 0; i < len(numbers)/2; i++ {
		j := len(numbers) - i - 1
		numbers[i], numbers[j] = numbers[j], numbers[i]
	}
	return numbers
}
