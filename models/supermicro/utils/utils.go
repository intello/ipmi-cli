package utils

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"ipmi-cli/interfaces"
	"ipmi-cli/web"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/fatih/color"
	"github.com/pkg/errors"
)

// ACL structs

// Ipmi represents the XML structure sent by the IPMI
type Ipmi struct {
	XMLName         xml.Name        `xml:"IPMI"`
	IPAccessControl ipAccessControl `xml:"IP_ACCESS_CONTROL"`
	ConfigInfo      ConfigInfo      `xml:"CONFIG_INFO"`
	SSLInfo         validateSSL     `xml:"SSL_INFO"`
	TimeInfo        timeInfo        `xml:"TIME"`
}

type ipAccessControl struct {
	FwRule []fwRule `xml:"FW_RULE"`
	State  string   `xml:"STATE,attr"`
}

type fwRule struct {
	Priority string `xml:"PRIORITY,attr"`
	IP       string `xml:"IP,attr"`
	Policy   string `xml:"POLICY,attr"`
}

// ACLToJSON for json parsing of ACL rules
type ACLToJSON struct {
	State string     `json:"state"`
	Rules []ACLRules `json:"rules"`
}

// ACLRules for json parsing of one ACL rule
type ACLRules struct {
	Priority string `json:"priority"`
	IP       string `json:"ipv4_cidr"`
	Policy   string `json:"policy"`
}

// User structs
type ConfigInfo struct {
	User         []user      `xml:"USER"`
	LAN          LanTag      `xml:"LAN"`
	DNS          DNSTag      `xml:"DNS"`
	LanInterface LANIfTag    `xml:"LAN_IF"`
	Hostname     HostnameTag `xml:"HOSTNAME"`
	MIILink      MIILinkTag  `xml:"LINK_INFO"`
}

type user struct {
	Name      string `xml:"NAME,attr"`
	Privilege int    `xml:"USER_ACCESS,attr"`
}

// UserInfoParam is used to return a user structure
type UserInfoParam struct {
	Name   string
	UserID string
}

// TLS
type validateSSL struct {
	Validate     sslInfo `xml:"VALIDATE"`
	ValidateAttr string  `xml:"VALIDATE,attr"`
}

type sslInfo struct {
	Cert string `xml:"CERT,attr"`
	Key  string `xml:"KEY,attr"`
}

// Time
type timeInfo struct {
	Time TimeInfoTag `xml:"INFO"`
}

type TimeInfoTag struct {
	NTP        string `xml:"NTP,attr"`
	NTP_Serv_1 string `xml:"NTP_SERVER_PRI,attr"`
	NTP_Serv_2 string `xml:"NTP_SERVER_2ND,attr"`
	Timezone   string `xml:"TIME_ZONE,attr"`
	DST        string `xml:"DST_EN,attr"` // Daylight savings time enabled? 0 = false
	Year       string `xml:"YEAR,attr"`
	Month      string `xml:"MONTH,attr"`
	Day        string `xml:"DAY,attr"`
	Hour       string `xml:"HOUR,attr"`
	Minute     string `xml:"MIN,attr"`
	Second     string `xml:"SEC,attr"`
}

// Network

type LanTag struct {
	BMC_IP    string `xml:"BMC_IP,attr"`
	BMCMask   string `xml:"BMC_MASK,attr"`
	GatewayIP string `xml:"GATEWAY_IP,attr"`
	DHCPEn    string `xml:"DHCP_EN,attr"`
	Subnet    string `xml:"BMC_NETMASK,attr"`
	VLAN_ID   string `xml:"VLAN_ID,attr"`
	RMCP_PORT string `xml:"RMCP_PORT,attr"`
}

type DNSTag struct {
	DNSServer string `xml:"DNS_SERVER,attr"`
}

type LanV6 struct {
	XMLName       xml.Name      `xml:"LAN_V6"`
	BMCIPV6Config BMCIPV6Config `xml:"BMC_IPV6"`
	DNSServer     IPV6DNSServer `xml:"DNS_SERVER"`
}

type BMCIPV6Config struct {
	Addr string `xml:"ADDR,attr"`
}

type IPV6DNSServer struct {
	IP string `xml:"IP,attr"`
}

type LANIfTag struct {
	Interface string `xml:"INTERFACE,attr"`
}

type HostnameTag struct {
	Name string `xml:"NAME,attr"`
}

type MIILinkTag struct {
	MIILinkConf string `xml:"MII_LINK_CONF"`
}

// User structs

// User to json
type userToJSON struct {
	Users []userInfo `json:"users"`
}

type userInfo struct {
	Name      string `json:"name"`
	Privilege string `json:"privilege"`
}

// GetACLRulesStruct returns a structure of the parsed XML for ACL Rules
func GetACLRulesStruct(body string) *Ipmi {
	var ipmiData *Ipmi
	xml.Unmarshal([]byte(body), &ipmiData)

	return ipmiData
}

// ExtractACLFromXML parses and formats the XML for the ACL acquired from the IPMI
func ExtractACLFromXML(body string, format string) (string, error) {
	var result string
	var jsonACL []byte
	var err error
	var ipmiData *Ipmi
	xml.Unmarshal([]byte(body), &ipmiData)

	if format == "text" {
		result += fmt.Sprintf("ACL State: %v\n", ipmiData.IPAccessControl.State)

		for _, value := range ipmiData.IPAccessControl.FwRule {
			result += fmt.Sprintf("%-2v IPv4 CIDR: %-18v Policy: %v\n", value.Priority, value.IP, value.Policy)
		}

		if result == fmt.Sprintf("ACL State: %v\n", ipmiData.IPAccessControl.State) {
			result += "No ACL rules present"
		}
	} else if format == "json" {
		myJSON := ACLToJSON{State: ipmiData.IPAccessControl.State}

		for _, value := range ipmiData.IPAccessControl.FwRule {
			myJSON.Rules = append(myJSON.Rules, ACLRules{Priority: value.Priority, IP: value.IP, Policy: value.Policy})
		}

		jsonACL, err = json.Marshal(myJSON)

		result = string(jsonACL)
	}

	return result, err
}

// ExtractACLRuleNoFromXML parses and formats the XML for the ACL acquired from the IPMI
func ExtractACLRuleNoFromXML(body string, ruleNumber int) string {
	var mode string = "add"
	var ipmiData *Ipmi
	xml.Unmarshal([]byte(body), &ipmiData)

	if ruleNumber <= len(ipmiData.IPAccessControl.FwRule) {
		mode = "modify"
	}

	return mode
}

// CheckIfRuleExists will parse the xml and verify if the ACL rule provided already exists
func CheckIfRuleExists(body string, rule string) bool {
	var ipmiData *Ipmi
	xml.Unmarshal([]byte(body), &ipmiData)

	for _, value := range ipmiData.IPAccessControl.FwRule {

		// Check both with and without a /32 to cover all versions of IPMIs
		if value.IP == rule+"/32" || value.IP == strings.TrimSuffix(rule, "/32") {
			return true
		}
	}

	return false
}

// CountNumberRules will return the number of rules from ACL
func CountNumberRules(body string) int {
	var ipmiData *Ipmi
	xml.Unmarshal([]byte(body), &ipmiData)

	return len(ipmiData.IPAccessControl.FwRule)
}

// ExtractUsersFromXML parses and formats the XML for the users acquired from the IPMI
func ExtractUsersFromXML(body string, format string) (string, error) {
	var result string
	var jsonUser []byte
	var err error
	var ipmiData *Ipmi
	xml.Unmarshal([]byte(body), &ipmiData)

	privilegeTranslation := [5]string{"No Access", "Error", "User", "Operator", "Administrator"}

	myJSON := userToJSON{}

	for _, value := range ipmiData.ConfigInfo.User {

		// Supermicro's IPMI has a default user with a username of 16 spaces
		if value.Name != "" && value.Name != "                " {
			if format == "text" {
				result += fmt.Sprintf("Username: %-15v Privilege: %v\n", value.Name, privilegeTranslation[value.Privilege])
			} else if format == "json" {
				myJSON.Users = append(myJSON.Users, userInfo{Name: value.Name, Privilege: privilegeTranslation[value.Privilege]})
				jsonUser, err = json.Marshal(myJSON)
				result = string(jsonUser)
			}
		}
	}

	// Remove last newline and return
	return strings.TrimSuffix(result, "\n"), err
}

// CheckIfUserExists will parse the xml, verify if the user provided already exists and return if found
func CheckIfUserExists(body string, username string) UserInfoParam {
	var result UserInfoParam
	var ipmiData *Ipmi
	xml.Unmarshal([]byte(body), &ipmiData)

	// This is the only way to find the user ID as far as I know
	count := 0

	for _, user := range ipmiData.ConfigInfo.User {
		count++

		// Usernames are case-sensitive
		if user.Name == username {
			result.Name = user.Name
			result.UserID = strconv.Itoa(count)

			return result
		}
	}

	return UserInfoParam{Name: ""}
}

// CountNumberUsersFromXML will return the number of users
func CountNumberUsersFromXML(body string) int {
	var ipmiData *Ipmi
	xml.Unmarshal([]byte(body), &ipmiData)

	count := 0

	for _, user := range ipmiData.ConfigInfo.User {
		if user.Name != "" {
			count++
		}
	}

	return count
}

// DeterminePrivilegeNumber returns the appropriate number
// according to its corresponding privilege string
func DeterminePrivilegeNumber(permission string) string {
	switch strings.ToLower(permission) {
	case "administrator":
		return "4"
	case "operator":
		return "3"
	case "user":
		return "2"
	default:
		color.RedString("Invalid permission: '%s'\n  User given no access\n", strings.ToLower(permission))
		return "0xf"
	}
}

// ProcessCertValidationResult will verify the certification files
func ProcessCertValidationResult(body string, IpmiVersion int) bool {
	var ipmiData *Ipmi
	xml.Unmarshal([]byte(body), &ipmiData)

	// Extract answers for the XML the IPMI sent
	cert, _ := strconv.Atoi(ipmiData.SSLInfo.Validate.Cert)
	key, _ := strconv.Atoi(ipmiData.SSLInfo.Validate.Key)
	validV2, _ := strconv.Atoi(ipmiData.SSLInfo.ValidateAttr)

	// Check if the key and cert file are valid
	if IpmiVersion == 1 {
		return cert == 1 && key == 1
	} else if IpmiVersion == 2 {
		return validV2 == 1
	}

	return false
}

// ExtractTimeFromXML parses and formats the XML for the time settings acquired from the IPMI
func ExtractTimeFromXML(body string) (string, error) {
	var result string
	var err error
	var ipmiData *Ipmi
	xml.Unmarshal([]byte(body), &ipmiData)

	var ntp_enabled = ipmiData.TimeInfo.Time.NTP != "0"
	var dst_enabled = ipmiData.TimeInfo.Time.DST != "0"
	var time = fmt.Sprintf("%s-%s-%s %s:%s:%s",
		ipmiData.TimeInfo.Time.Year,
		ipmiData.TimeInfo.Time.Month,
		ipmiData.TimeInfo.Time.Day,
		ipmiData.TimeInfo.Time.Hour,
		ipmiData.TimeInfo.Time.Minute,
		ipmiData.TimeInfo.Time.Second,
	)

	// Calculate timezone
	var timezoneInt int
	var timezone string
	timezoneInt, _ = strconv.Atoi(ipmiData.TimeInfo.Time.Timezone)

	if timezoneInt >= 0 {
		timezone = fmt.Sprintf("UTC+%s", strconv.Itoa(timezoneInt/3600))
	} else {
		timezone = fmt.Sprintf("UTC%s", strconv.Itoa(timezoneInt/3600))
	}

	result +=
		"Current time settings :\n" +
			"---\n" +
			fmt.Sprintf("NTP enabled?                   %t\n", ntp_enabled) +
			fmt.Sprintf("Daylight savings time enabled? %t\n", dst_enabled) +
			fmt.Sprintf("Time:                          %s\n", time) +
			fmt.Sprintf("Timezone:                      %s\n", timezone) +
			fmt.Sprintf("NTP Server 1:                  %s\n", ipmiData.TimeInfo.Time.NTP_Serv_1) +
			fmt.Sprintf("NTP Server 2:                  %s", ipmiData.TimeInfo.Time.NTP_Serv_2)

	return result, err
}

func GetTimeValuesFromIPMI(options *interfaces.CLIOptions) (TimeInfoTag, error) {
	var err error
	var ipmiData *Ipmi

	// If a value is empty, use the current value from IPMI
	data := url.Values{}
	if options.Version == "v1" {
		data.Set("GET_DATE_TIME.XML", "(0,0)")
		data.Set("time_stamp", time.Now().String())
	} else if options.Version == "v2" {
		data.Set("op", "GET_DATE_TIME.XML")
		data.Set("r", "(0,0)")
	}

	resp, err := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))
	if err != nil {
		return ipmiData.TimeInfo.Time, errors.Wrap(err, "Error getting time settings")
	}

	xml.Unmarshal([]byte(resp.Body), &ipmiData)

	return ipmiData.TimeInfo.Time, err
}

// ExtractTimeFromXML parses and formats the XML for the network settings acquired from the IPMI
func ExtractNetworkFromXML(body string) (string, error) {
	var result string
	var err error
	var ipmiData *Ipmi
	xml.Unmarshal([]byte(body), &ipmiData)

	dhcpEnabled, _ := oneOrZeroToBool(ipmiData.ConfigInfo.LAN.DHCPEn)

	result +=
		"Current network settings :\n" +
			"---\n" +
			fmt.Sprintf("DHCP enabled?:        %s\n", strconv.FormatBool(dhcpEnabled)) +
			fmt.Sprintf("IPMI IP:              %s\n", stripLeadingZeroFromIP(ipmiData.ConfigInfo.LAN.BMC_IP)) +
			fmt.Sprintf("IPMI Network Mask:    %s\n", stripLeadingZeroFromIP(ipmiData.ConfigInfo.LAN.Subnet)) +
			fmt.Sprintf("IPMI Default Gateway: %s\n", stripLeadingZeroFromIP(ipmiData.ConfigInfo.LAN.GatewayIP)) +
			fmt.Sprintf("DNS:                  %s\n", stripLeadingZeroFromIP(ipmiData.ConfigInfo.DNS.DNSServer))

	return result, err
}

func stripLeadingZeroFromIP(ip string) string {
	stripped := ""
	num := strings.Split(ip, ".")

	for _, el := range num {
		temp := strings.TrimPrefix(el, "0") // Strip first leading 0 if there is one

		if len(temp) == 0 {
			stripped += "0"
			return stripped
		}

		if temp[0] == 48 {
			temp = strings.TrimPrefix(temp, "0") // Strip second leading 0 if there is one
		}

		stripped += temp + "."
	}
	stripped = strings.TrimSuffix(stripped, ".") // Remove last dot

	return stripped
}

func GetNetworkValuesFromIPMI(options *interfaces.CLIOptions) (ConfigInfo, error) {
	var err error
	var ipmiData *Ipmi

	// If a value is empty, use the current value from IPMI
	data := url.Values{}
	if options.Version == "v1" {
		data.Set("CONFIG_INFO.XML", "(0,0)")
		data.Set("time_stamp", time.Now().String())
	} else if options.Version == "v2" {
		data.Set("op", "CONFIG_INFO.XML")
		data.Set("r", "(0,0)")
	}

	resp, err := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))
	if err != nil {
		return ipmiData.ConfigInfo, errors.Wrap(err, "Error getting network settings")
	}

	xml.Unmarshal([]byte(resp.Body), &ipmiData)

	return ipmiData.ConfigInfo, err
}

func GetNetworkIPv6ValuesFromIPMI(options *interfaces.CLIOptions) (LanV6, error) {
	var err error
	var ipmiData *LanV6

	data := url.Values{}
	if options.Version == "v1" {
		data.Set("CONFIG_IPV6.XML", "(0,0)")
		data.Set("time_stamp", time.Now().String())
	} else if options.Version == "v2" {
		data.Set("op", "CONFIG_IPV6.XML")
		data.Set("r", "(0,0)")
	}

	resp, err := web.Post("ipmi.cgi", strings.NewReader(data.Encode()))
	if err != nil {
		return *ipmiData, errors.Wrap(err, "Error getting IPv6 settings")
	}
	xml.Unmarshal([]byte(resp.Body), &ipmiData)

	return *ipmiData, err
}

func oneOrZeroToBool(num string) (bool, error) {
	if num == "1" {
		return true, nil
	} else if num == "0" {
		return false, nil
	}

	return false, errors.New(fmt.Sprintf("couldn't convert [%s] into a bool", num))
}
