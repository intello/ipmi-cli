package version

import (
	_ "errors"
	"fmt"
	_ "ipmi-cli/errhandler"
	"ipmi-cli/interfaces"
	"ipmi-cli/models/asrock/version/common"

	"github.com/fatih/color"
)

// Handle will proceed with the correct methods for a given version
func Handle(options *interfaces.CLIOptions) (string, error) {
	var result string
	var err error = nil

	result, err = handleCommand(options)

	return result, err
}

// handleCommand will handle the command passed
// from the CLI according to this IPMI version's required parameters.
func handleCommand(options *interfaces.CLIOptions) (string, error) {
	var result string
	var err error

	switch options.Command {
	case "acl":
		switch options.Subcommand {
		case "list":
			return common.ListACL(options)
		case "set":
			//return common.SetACL(options), nil
		case "update":
			//return common.UpdateACL(options)
		case "delete":
			//return common.DeleteRule(options.RuleNumber, options.Version), nil
		case "deleteall":
			//return common.DeleteAllRules(options), nil
		}
	default:
		err = fmt.Errorf(color.RedString("Sorry, some commands are not defined yet for this model"))
	}
	return result, err
}
