package common

import (
	"encoding/xml"
	"io/ioutil"
	"ipmi-cli/htmlparser"
	"ipmi-cli/logger"
	"ipmi-cli/web"
	"net/url"
	"regexp"
	"strings"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

func sendDataToIPMI(data url.Values) string {
	resp, err := web.Post("session", strings.NewReader(data.Encode()))

	if err != nil {
		logger.Log(errors.Wrap(err, "Sending data failed").Error(), logrus.InfoLevel)
	}

	var validation *ipmi
	xml.Unmarshal([]byte(resp.Body), &validation)

	if validation.XMLName.Local == "" {
		// To support IPMI firmwares 1.60+
		CSRFToken := getCSRFToken("config_ip_ctrl")

		if CSRFToken != "" {
			web.ChangeCSRFToken(CSRFToken)

			var validation *ipmi
			xml.Unmarshal([]byte(resp.Body), &validation)
		}
	}

	return validation.XMLName.Local
}

func getCSRFToken(page string) string {
	resp, _ := web.Get("url_redirect.cgi?url_name=" + page)
	reader := strings.NewReader(resp.Body)
	startMatch := htmlparser.NewSkipTillReader(reader, []byte("<script>SmcCsrfInsert"))
	content := htmlparser.NewReadTillReader(startMatch, []byte("</script>"))
	bs, _ := ioutil.ReadAll(content)

	r := regexp.MustCompile(`\<script\>SmcCsrfInsert \(\"CSRF_TOKEN\", \"(.*)\"\);\</script\>`)
	token := r.FindStringSubmatch(string(bs))

	// If no token is found, return empty string
	if len(token) == 0 {
		return ""
	}

	return token[1]
}
