package common

import (
	"ipmi-cli/interfaces"
	"ipmi-cli/models/asrock/utils"
	"ipmi-cli/web"

	"github.com/pkg/errors"
)

// ListACL will lists the access control list from the API
func ListACL(options *interfaces.CLIOptions) (string, error) {

	resp, err := web.Get("settings/firewall-ip-rules")
	if err != nil {
		return "", errors.Wrap(err, "Error getting ACL")
	}

	return utils.ExtractACLFromJSON(resp.Body, options.Output)
}
