package common

import "encoding/xml"

type ipmi struct {
	XMLName  xml.Name `xml:"IPMI"`
	BMCRESET struct {
		STATE struct {
			CODE string `xml:"CODE,attr"`
		} `xml:"STATE"`
	} `xml:"BMC_RESET"`
}
