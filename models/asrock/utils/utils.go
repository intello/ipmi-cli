package utils

import (
	"encoding/json"
	"fmt"
)

// ACLRule for json parsing of one ACL rule
type ACLRule struct {
	ID        int    `json:"id"`
	IPStart   string `json:"ip_start"`
	IPEnd     string `json:"ip_end"`
	Rule      string `json:"rule"`
	Timeout   int    `json:"timeout"`
	StartTime int    `json:"start_time"`
	EndTime   int    `json:"end_time"`
}

// ExtractACLFromJSON parses and formats the JSON for the ACL acquired from the IPMI
func ExtractACLFromJSON(body string, format string) (string, error) {
	var result string
	var err error
	var rules []ACLRule
	json.Unmarshal([]byte(body), &rules)

	if len(rules) == 0 {
		result += "No ACL rules present"
	} else {
		for _, content := range rules {
			result += fmt.Sprintf("%-2v IP range: %s - %s Policy: %s\n", content.ID, content.IPStart, content.IPEnd, content.Rule)
		}
	}

	return result, err
}
