package asrock

import (
	"encoding/json"
	"encoding/xml"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"ipmi-cli/interfaces"
	"ipmi-cli/logger"
	"ipmi-cli/models/asrock/version"
	"ipmi-cli/web"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

var sid string

// The following structs are required for XML parsing through the encoding/xml package
type ipmi struct {
	XMLName     xml.Name    `xml:"IPMI"`
	GenericInfo genericinfo `xml:"GENERIC_INFO"`
}

type genericinfo struct {
	Generic generic `xml:"GENERIC"`
}

type generic struct {
	IpmiFWTag     string `xml:"IPMIFW_TAG,attr"`
	IpmiFWVersion string `xml:"IPMIFW_VERSION,attr"`
}

type loginResponse struct {
	Duplicate     int
	RacID         int
	OK            string
	Privilege     string
	ExtendedPriv  string
	RacSession_ID string
	Remote_Addr   string
	Server_Name   string
	Server_Addr   string
	HTTPSEnabled  string
	CSRFToken     string
}

// Init will init the AsRock models
func Init(options *interfaces.CLIOptions) (string, error) {
	var result string = ""

	webOptions := web.ClientOptions{
		BaseURL:               options.Host,
		APIPath:               "api",
		Cookies:               make(map[string]string),
		ContentType:           "application/x-www-form-urlencoded",
		CertificateValidation: true,
	}

	web.Init(webOptions)

	// Login and get QSESSIONID from cookies, CSTFToken from response
	sessionID, CSRFToken := getSessionID(options.Host, options.LoginUser, options.LoginPassword)

	if sessionID == "" {
		return result, errors.New("Unable to log into the IPMI, please verify credentials")
	}

	web.AddCookie("QSESSIONID", sessionID)
	web.ChangeCSRFToken(CSRFToken)

	return version.Handle(options)
}

func getSessionID(host string, user string, password string) (string, string) {

	// prepare data to send
	data := url.Values{}
	data.Set("username", user)
	data.Set("password", password)
	data.Set("dup_racid", "0")

	resp, err := web.Post("session", strings.NewReader(data.Encode()))
	if err != nil {
		logger.Log(errors.Wrap(err, "Login page of IPMI unreachable").Error(), logrus.InfoLevel)
	}

	var loginResponse loginResponse
	json.Unmarshal([]byte(resp.Body), &loginResponse)

	// if we receive a response with 'duplicate' set to 1, we must log in again, passing the value of 'racid'
	duplicate := loginResponse.Duplicate
	sidValue := ""

	if duplicate == 1 {
		racID := strconv.Itoa(loginResponse.RacID)

		logger.Log("Duplicate login detected", logrus.InfoLevel)

		data.Set("dup_racid", racID)
		resp, err := web.Post("session", strings.NewReader(data.Encode()))
		if err != nil {
			logger.Log(errors.Wrap(err, "Login page of IPMI unreachable").Error(), logrus.InfoLevel)
		}
		sidValue = findSID(resp.Cookies)

		json.Unmarshal([]byte(resp.Body), &loginResponse)
	} else {
		sidValue = findSID(resp.Cookies)
	}
	CSRFToken := loginResponse.CSRFToken
	logger.Log("Got CSRFToken: "+CSRFToken, logrus.InfoLevel)

	return sidValue, CSRFToken
}

func findSID(cookies []*http.Cookie) string {
	sidValue := ""
	for _, cookie := range cookies {
		logger.Log("Cookie name: "+cookie.Name, logrus.InfoLevel)
		logger.Log("Cookie value: "+cookie.Value, logrus.InfoLevel)

		if cookie.Value != "" && cookie.Name == "QSESSIONID" {
			sidValue = cookie.Value
			break
		}
	}
	return sidValue
}
