package main

import (
	"ipmi-cli/cmd"
	"ipmi-cli/logger"

	"github.com/sirupsen/logrus"
)

func main() {
	// Init at error level
	logger.Init(logrus.ErrorLevel.String())

	cmd.Execute()
}
