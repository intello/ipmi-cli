package errhandler

import (
	"fmt"
	"os"

	"github.com/fatih/color"
)

// ExitIfErr will exit if an error is found
func ExitIfErr(err error) {
	if err != nil {
		fmt.Println(color.RedString("%v\n", err))
		os.Exit(1)
	}
}
