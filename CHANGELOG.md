# Changelog

All notable changes to this project will be documented in this file.

NOTE: The project follows [Semantic Versioning](http://semver.org/).

# v1.1.0 - May 28th, 2021

[enhancement] #4 - Configure DNS and Date

# v1.0.0 - November 24th, 2020

Initial public release
